#script to implement the base element that contains attribute for the execution

from __future__ import annotations
from typing import Dict, List, Set, Union

import sys

class BaseElement:
	#nSteps : int #number of steps to complete the calculus
	#initialState : Dict[str, float]
	#finalState : Dict[str, float]

	def __init__(self) -> None:
		#inizialize the nSteps 
		#self.nSteps = 0
		pass

	def toStringTree(self, nTab=0) -> str:
		#method that will be override into classes to return the string of the tree
		#to avoid not implementation this method raise exception
		raise Exception('method toStringTree not implemented')

	def toGraphicTree(self):
		#method that will be override into classes to return the graphic of the tree
		#to avoid not implementation this method raise exception
		raise Exception('method toGraphicTree not implemented')

	def toPlainString(self) -> str:
		#method to convert the element to a plain string in a single line
		plainString = self.toStringTree(nTab=0).replace('\t', '').replace('\n', '')
		return plainString

	def findVariableSet(self) -> Set[str]:
		#method to find the set that contains all the variables name. it is used in the for statement
		#will be overwritten in the children
		#to avoid not implementation this method raise exception
		raise Exception('method findVariableSet not implemented')