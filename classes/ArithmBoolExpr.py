#script to implement the arithmetic boolean expression

from __future__ import annotations
from classes.ExpressionBaseElement import ExpressionBaseElement

from typing import Dict, List, Set, Tuple, Union
from classes.ArithmExpr import ArithmExpr
import classes.AtomBoolExpr as AtomBoolExpr
import classes.AndBoolExpr as AndBoolExpr
import classes.OrBoolExpr as OrBoolExpr


class ArithmBoolExpr(ExpressionBaseElement):
	#types to identify the symbol of equation/disequation
	typeEq = 0
	typeNE = 1
	typeLT = 2
	typeLE = 3
	typeGT = 4
	typeGE = 5

	strEq = '=='
	strNE = '|='
	strLT = '<'
	strLE = '<='
	strGT = '>'
	strGE = '>='

	idType : int

	#the expression of the leftside and rightside
	leftArithmExpr : ArithmExpr
	rightArithmExpr : ArithmExpr

	def __init__(self, leftAExpr:ArithmExpr, strOp:str, rightAExpr:ArithmExpr) -> None:
		#the parameter is a tuple like it appears in the string
		super().__init__()
		self.leftArithmExpr = leftAExpr 
		self.rightArithmExpr = rightAExpr
		#verify the type
		if strOp == self.strEq:
			self.idType = self.typeEq
		elif strOp == self.strNE:#'≠'
			self.idType = self.typeNE
		elif strOp == self.strLT:
			self.idType = self.typeLT
		elif strOp == self.strLE:#'≤'
			self.idType = self.typeLE
		elif strOp == self.strGT:
			self.idType = self.typeGT
		elif strOp == self.strGE:#'≥'
			self.idType = self.typeGE
		else:
			raise Exception('ArithmBoolExpr with type of operation not valid')

	def findVariableSet(self) -> Set[str]:
		variableSet = self.leftArithmExpr.findVariableSet()
		variableSet.update(self.rightArithmExpr.findVariableSet())
		return variableSet

	def toStringTree(self, nTab=0) -> str:
		#method to convert the arithmetic boolean expression to a string
		#start with the number of tabs
		baseTab = '\t'*nTab
		stringTree = baseTab
		stringTree += 'ABExp[' + '\n'
		#left expression
		stringTree += self.leftArithmExpr.toStringTree(nTab=nTab+1)
		#operator
		stringTree += baseTab + '\t'
		if self.idType == self.typeEq:
			stringTree += ' == '
		elif self.idType == self.typeNE:
			stringTree += ' != '
		elif self.idType == self.typeLT:
			stringTree += ' < '
		elif self.idType == self.typeLE:
			stringTree += ' <= '
		elif self.idType == self.typeGT:
			stringTree += ' > '
		elif self.idType == self.typeGE:
			stringTree += ' >= '
		stringTree += '\n'
		#right expression
		stringTree += self.rightArithmExpr.toStringTree(nTab=nTab+1)
		#final string
		stringTree += baseTab + ']' + '\n'
		return stringTree

	def evaluate(self, state: Dict[str, float], listExecState: Union[List[str], None], flagReduction: bool) -> bool:
		#method to evaluate the arithmetic boolean expression
		if flagReduction:
			return self.evaluateReduction(state=state, listExecState=listExecState)
		else:
			return self.evaluateNotReduction(state=state, listExecState=listExecState)

	def evaluateReduction(self, state: Dict[str, float], listExecState: Union[List[str], None]) -> bool:
		#method to evaluate the arithmetic boolean expression with Reduction mode -> transform into a base case
		#the direct evaluation is compute only for state equal and less or equal, other cases are transformed
		if self.idType in (self.typeEq, self.typeLE):
			#create a string to evaluate
			stringToEvaluate = ''
			#left expression
			stringToEvaluate += str(self.leftArithmExpr.evaluateProcess(state=state, listExecState=listExecState, typeExpr=ExpressionBaseElement.typeEvalAExpr, flagAtomic=False, flagReduction=True))
			#distinguish the case
			if self.idType == self.typeEq:
				stringToEvaluate += ' == '
			elif self.idType == self.typeLE:
				stringToEvaluate += ' <= '
			#right expression
			stringToEvaluate += str(self.rightArithmExpr.evaluateProcess(state=state, listExecState=listExecState, typeExpr=ExpressionBaseElement.typeEvalAExpr, flagAtomic=False, flagReduction=True))
			#evaluate the string
			return eval(stringToEvaluate)
		#otherwise is a case that can be transformed into another case
		#create the not equal statement that can be used in next step
		equalABExpr = ArithmBoolExpr(leftAExpr=self.leftArithmExpr, strOp=self.strEq, rightAExpr=self.rightArithmExpr)
		#create the arithmetic boolean expression
		atomEqualABExpr = AtomBoolExpr.AtomBoolExpr(par=equalABExpr)
		#create the not equal
		atomNEqualABExpr = AtomBoolExpr.AtomBoolExpr(atomEqualABExpr)
		#create the and statement 
		if self.idType == self.typeNE:
			#evaluation of thye not equal
			result = atomNEqualABExpr.evaluateProcess(state=state, listExecState=listExecState, typeExpr=ExpressionBaseElement.typeEvalBExpr, flagAtomic=False, flagReduction=True)
			assert type(result) is bool
			return result
		#calculate the and statement for not equal
		andNEqualABExpr = AndBoolExpr.AndBoolExpr(leftBExpr=atomNEqualABExpr)
		#now it is calculated a less equal boolean expression
		lessEqualABExpr = ArithmBoolExpr(leftAExpr=self.leftArithmExpr, strOp=self.strLE, rightAExpr=self.rightArithmExpr)
		#calculate the atomic boolean expression
		atomLEABExpr = AtomBoolExpr.AtomBoolExpr(lessEqualABExpr)
		#check for the less than type
		if self.idType == self.typeLT:
			#the < is equal to: A<B -> A<=B && A!=B
			andLessThanBoolExpr = AndBoolExpr.AndBoolExpr(leftBExpr=atomLEABExpr, rightBExpr=andNEqualABExpr)
			result = andLessThanBoolExpr.evaluateProcess(state=state, listExecState=listExecState, typeExpr=ExpressionBaseElement.typeEvalBExpr,flagAtomic=False, flagReduction=True)
			assert type(result) is bool
			return result
		#calculate the not less or equal
		atomNotLEABExpr = AtomBoolExpr.AtomBoolExpr(atomLEABExpr)
		if self.idType == self.typeGT:
			#the greater is equal to: A>B -> not A <=B
			result = atomNotLEABExpr.evaluateProcess(state=state, listExecState=listExecState, typeExpr=ExpressionBaseElement.typeEvalBExpr,flagAtomic=False, flagReduction=True)
			assert type(result) is bool 
			return result
		#calculate the and for the not less or equal
		andNotLEABExpr = AndBoolExpr.AndBoolExpr(leftBExpr=atomNotLEABExpr)
		#calculate the or for the equal statement
		#first calculate the and boolean expression
		andEqualABExpr = AndBoolExpr.AndBoolExpr(leftBExpr=atomEqualABExpr)
		orEqualABExpr = OrBoolExpr.OrBoolExpr(leftBExpr=andEqualABExpr)
		#if self.idType == self.typeGE:
		#the greater or equal is equivalent to: A>=B -> not(A<=B) or A==B
		orGEABExpr = OrBoolExpr.OrBoolExpr(leftBExpr=andNotLEABExpr, rightBExpr=orEqualABExpr)
		result = orGEABExpr.evaluateProcess(state=state, listExecState=listExecState, flagAtomic=False, flagReduction=True, typeExpr=ExpressionBaseElement.typeEvalBExpr)
		assert type(result) is bool 
		return result

	def evaluateNotReduction(self, state: Dict[str, float], listExecState: Union[List[str], None]) -> bool:
		#method to evaluate the arithmetic boolean expression without Reduction mode in the minimum number of execution
		#eval the direct evaluation
		#create a string to evaluate
		stringToEvaluate = ''
		#left expression
		stringToEvaluate += str(self.leftArithmExpr.evaluateProcess(state=state, listExecState=listExecState, typeExpr=ExpressionBaseElement.typeEvalAExpr, flagAtomic=False, flagReduction=False))
		#distinguish the case
		if self.idType == self.typeEq:
			stringToEvaluate += ' == '
		elif self.idType == self.typeNE:
			stringToEvaluate += ' != '
		elif self.idType == self.typeLT:
			stringToEvaluate += ' < '
		elif self.idType == self.typeLE:
			stringToEvaluate += ' <= '
		elif self.idType == self.typeGT:
			stringToEvaluate += ' > '
		elif self.idType == self.typeGE:
			stringToEvaluate += ' >= '
		#right expression
		stringToEvaluate += str(self.rightArithmExpr.evaluateProcess(state=state, listExecState=listExecState, typeExpr=ExpressionBaseElement.typeEvalAExpr, flagAtomic=False, flagReduction=False))
		#evaluate the string
		return eval(stringToEvaluate)

	#########################################################################################################################################################
	#method for graphic 

	def toGraphicTree(self):
		#method to convert the arithmetic boolean expression to a graphic tree
		from PyQt5 import QtWidgets
		graphicTree = QtWidgets.QTreeWidgetItem()
		graphicTree.setText(0, 'ABExp')
		#left expression
		firstChildTree = self.leftArithmExpr.toGraphicTree()
		graphicTree.addChild(firstChildTree)
		#operator
		if self.idType == self.typeEq:
			opChild = QtWidgets.QTreeWidgetItem()
			opChild.setText(0, '==')
			graphicTree.addChild(opChild)
		elif self.idType == self.typeNE:
			opChild = QtWidgets.QTreeWidgetItem()
			opChild.setText(0, '!=')
			graphicTree.addChild(opChild)
		elif self.idType == self.typeLT:
			opChild = QtWidgets.QTreeWidgetItem()
			opChild.setText(0, '<')
			graphicTree.addChild(opChild)
		elif self.idType == self.typeLE:
			opChild = QtWidgets.QTreeWidgetItem()
			opChild.setText(0, '<=')
			graphicTree.addChild(opChild)
		elif self.idType == self.typeGT:
			opChild = QtWidgets.QTreeWidgetItem()
			opChild.setText(0, '>')
			graphicTree.addChild(opChild)
		elif self.idType == self.typeGE:
			opChild = QtWidgets.QTreeWidgetItem()
			opChild.setText(0, '>=')
			graphicTree.addChild(opChild)
		#right expression
		secondChildTree = self.rightArithmExpr.toGraphicTree()
		graphicTree.addChild(secondChildTree)
		return graphicTree

