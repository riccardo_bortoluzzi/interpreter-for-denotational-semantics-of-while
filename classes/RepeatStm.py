#script to implement the repeat until statement

from __future__ import annotations
from classes.ExpressionBaseElement import ExpressionBaseElement
from classes.OrBoolExpr import OrBoolExpr
from classes.AndBoolExpr import AndBoolExpr
from classes.AtomBoolExpr import AtomBoolExpr
from typing import Dict, List, Set, Union
from classes.StatementBaseElement import StatementBaseElement
import classes.WhileStm as WhileStm
import classes.AtomStatement as AtomStatement

from classes.BoolExpr import BoolExpr
import classes.Statement as Statement

class RepeatStm(StatementBaseElement):
	guard : BoolExpr
	stm : Statement.Statement
	numberOfExecution : int #number of execution in the non reduction mode

	def __init__(self, stm:Statement.Statement, guard:BoolExpr) -> None:
		#method to inizialize the object and populate the attributes
		super().__init__()
		self.guard = guard
		self.stm = stm

	def toStringTree(self, nTab=0) -> str:
		#method to convert the repeat until statement to a string
		#start with the number of tabs
		baseTab = '\t'*nTab
		stringTree = baseTab + 'repeat ' + '\n'
		stringTree += self.stm.toStringTree(nTab=nTab+1)
		stringTree += baseTab + ' until ' + '\n'
		stringTree += self.guard.toStringTree(nTab=nTab+1)
		return stringTree

	def findVariableSet(self) -> Set[str]:
		variableSet = self.guard.findVariableSet()
		variableSet.update(self.stm.findVariableSet())
		return variableSet

	def execute(self, state: Dict[str, float], listShowWhile: Union[List[str], None], listExecState: Union[List[str], None], flagReduction: bool, variableSet: Set[str]) -> None:
		#verify if flag Reduction is selected
		if flagReduction:
			self.executeReduction(state=state, listShowWhile=listShowWhile, listExecState=listExecState, variableSet=variableSet)
		else:
			self.executeNotReduction(state=state, listShowWhile=listShowWhile, listExecState=listExecState, variableSet=variableSet)

	def executeReduction(self, state: Dict[str, float], listShowWhile: Union[List[str], None], listExecState: Union[List[str], None], variableSet: Set[str]) -> None:
		#to execute the repeat until statement it is transformed into a while statement in Reduction mode
		#the guard have to be transformed into a not guard
		atomParGuardBExpr = AtomBoolExpr(self.guard)
		atomNotGuardBExpr = AtomBoolExpr(atomParGuardBExpr)
		andNotGuardBExpr = AndBoolExpr(leftBExpr=atomNotGuardBExpr)
		orNotGuardBExpr = OrBoolExpr(leftBExpr=andNotGuardBExpr)
		notGuardBExpr = BoolExpr(leftBExpr=orNotGuardBExpr)
		#create the atom statement of the while statement
		whileStatement = WhileStm.WhileStm(guard=notGuardBExpr, stm=self.stm)
		atomWhileStatement = AtomStatement.AtomStatement(whileStatement)
		#create the new statement to execute that includes the statement of the repeat until and 
		listNewStatement = self.stm.listStms[:]
		listNewStatement.append(atomWhileStatement)
		newEquivalentStatement = Statement.Statement(listStms=listNewStatement)
		#execute it
		newEquivalentStatement.executionProcess(state=state, listShowWhile=listShowWhile, listExecState=listExecState, flagReduction=True, variableSet=variableSet,flagAtomic=False)

	def executeNotReduction(self, state: Dict[str, float], listShowWhile: Union[List[str], None], listExecState: Union[List[str], None], variableSet: Set[str]) -> None:
		#to execute the repeat until statement whit the least logic: execute statement and go to a while
		self.numberOfExecution = 1
		self.stm.executionProcess(state=state, listShowWhile=listShowWhile, listExecState=listExecState, flagReduction=False, variableSet=variableSet, flagAtomic=False)
		#now execute the while
		while not(self.guard.evaluateProcess(state=state, listExecState=listExecState, typeExpr=ExpressionBaseElement.typeEvalBExpr, flagAtomic=False, flagReduction=False)):
			#increment the number of execution
			self.numberOfExecution += 1
			#recall the statement
			self.stm.executionProcess(state=state, listShowWhile=listShowWhile, listExecState=listExecState, flagReduction=False, variableSet=variableSet,flagAtomic=False)

	#########################################################################################################################################################
	#method for graphic 

	def toGraphicTree(self):
		#method to convert the repeat until statement to a graphic tree
		from PyQt5 import QtWidgets
		graphicTree = QtWidgets.QTreeWidgetItem()
		graphicTree.setText(0, 'RepeatUntilStm')
		repeatChildTree = QtWidgets.QTreeWidgetItem()
		graphicTree.addChild(repeatChildTree)
		childTreeStm =  self.stm.toGraphicTree()
		repeatChildTree.addChild(childTreeStm)
		untilChildTree = QtWidgets.QTreeWidgetItem()
		untilChildTree.setText(0, 'until')
		guardChildTree = self.guard.toGraphicTree()
		untilChildTree.addChild(guardChildTree)
		graphicTree.addChild(repeatChildTree)
		graphicTree.addChild(untilChildTree)
		return graphicTree 


