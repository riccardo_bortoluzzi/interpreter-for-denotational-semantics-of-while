#script to implement boolean expression in general with and, or, implies and double implies

from __future__ import annotations
from classes.ExpressionBaseElement import ExpressionBaseElement
import classes.OrBoolExpr as OrBoolExpr
import classes.AndBoolExpr as AndBoolExpr
import classes.AtomBoolExpr as AtomBoolExpr

from typing import Dict, List, Set, Tuple, Union


class BoolExpr(ExpressionBaseElement):
	#type to identify the operation
	typeOpNone = 0
	typeOpImplies = 1
	typeOpDoubleImplies = 2

	typeId : int
	#listAndOrBExpr : List[AndOrBoolExpr.AndOrBoolExpr]
	leftBoolExpr : OrBoolExpr.OrBoolExpr
	rightBoolExpr : BoolExpr



	def __init__(self, leftBExpr : OrBoolExpr.OrBoolExpr, strOp:str = '', rightBExpr : Union[BoolExpr, None] = None) -> None:
		#method to inizialize and calculate the type of operator
		super().__init__()
		self.leftBoolExpr = leftBExpr
		if strOp == '':
			self.typeId = self.typeOpNone
		else:
			#there is the second expression -> populate the right boolean expression
			assert rightBExpr is not None
			self.rightBoolExpr = rightBExpr
			if strOp == '->':
				self.typeId = self.typeOpImplies
			elif strOp == '<->':
				self.typeId = self.typeOpDoubleImplies
			else:
				raise Exception('BoolExpr with type operation no valid')

	def toStringTree(self, nTab=0) -> str:
		#method to convert the boolean expression to a string
		#start with the number of tabs
		baseTab = '\t'*nTab
		if self.typeId == self.typeOpNone:
			stringTree =  self.leftBoolExpr.toStringTree(nTab=nTab)
		else:
			stringTree = baseTab + 'BExp[' + '\n'
			stringTree += self.leftBoolExpr.toStringTree(nTab=nTab+1)
			#insert the second expression
			stringTree += baseTab + '\t'
			if self.typeId == self.typeOpImplies:
				stringTree += ' -> '
			elif self.typeId == self.typeOpDoubleImplies:
				stringTree += ' <-> '
			stringTree += '\n'
			stringTree += self.rightBoolExpr.toStringTree(nTab=nTab+1)
			#close the bExp
			stringTree += baseTab + ']' + '\n'
		return stringTree

	def findVariableSet(self) -> Set[str]:
		variableSet = self.leftBoolExpr.findVariableSet()
		if self.typeId != self.typeOpNone:
			variableSet.update(self.rightBoolExpr.findVariableSet())
		return variableSet

	def evaluate(self, state: Dict[str, float], listExecState: Union[List[str], None], flagReduction: bool) -> bool:
		#method to evaluate the boolean expression
		#distinguish the case
		if flagReduction:
			return self.evaluateReduction(state=state, listExecState=listExecState)
		else:
			return self.evaluateNotReduction(state=state, listExecState=listExecState)

	def evaluateReduction(self, state: Dict[str, float], listExecState: Union[List[str], None]) -> bool:
		#method to evaluate the boolean expression with Reduction -> reduce to base case
		#verify the case
		if self.typeId == self.typeOpNone:
			#the expression contains only a value -> execute it
			result = self.leftBoolExpr.evaluateProcess(state=state, listExecState=listExecState, typeExpr=ExpressionBaseElement.typeEvalBExpr, flagAtomic=True, flagReduction=True)
			assert type(result) is bool
			return result
		#otherwise it an implication 
		#A->B : !A or B 
		#A <-> B : (A and B) or (!A and !B) = (A and B) or (!B and !A)
		#calculate the not A in an and expression
		leftBoolExpr = BoolExpr(leftBExpr=self.leftBoolExpr)
		leftParAtomBExpr = AtomBoolExpr.AtomBoolExpr(leftBoolExpr)
		leftNotBExpr = AtomBoolExpr.AtomBoolExpr(leftParAtomBExpr)
		leftAndNotBExpr = AndBoolExpr.AndBoolExpr(leftBExpr=leftNotBExpr)
		#create the or expression for the second
		rightParAtomBExpr = AtomBoolExpr.AtomBoolExpr(self.rightBoolExpr)
		rightAndBExpr = AndBoolExpr.AndBoolExpr(leftBExpr=rightParAtomBExpr)
		rightOrBExpr = OrBoolExpr.OrBoolExpr(leftBExpr=rightAndBExpr)
		if self.typeId == self.typeOpImplies:
			#A->B : !A or B 
			#create the or object
			orImpliesBExpr = OrBoolExpr.OrBoolExpr(leftBExpr=leftAndNotBExpr, rightBExpr=rightOrBExpr)
			result = orImpliesBExpr.evaluateProcess(state=state, listExecState=listExecState, typeExpr=ExpressionBaseElement.typeEvalBExpr, flagAtomic=False, flagReduction=True)
			assert type(result) is bool
			return result
		#otherwise it is the double implication
		#A <-> B : (A and B) or (!A and !B) = (A and B) or (!B and !A)
		#prepare the first and
		firstAndDoubleImpliesBExpr = AndBoolExpr.AndBoolExpr(leftBExpr=leftParAtomBExpr, rightBExpr=rightAndBExpr)
		#prepare the !B like a atom boolean express
		rightNotBExpr = AtomBoolExpr.AtomBoolExpr(rightParAtomBExpr)
		#prepare the second and
		secondAndDoubleImpliesBExpr = AndBoolExpr.AndBoolExpr(leftBExpr=rightNotBExpr, rightBExpr=leftAndNotBExpr)
		secondOrDoubleImpliesBExpr = OrBoolExpr.OrBoolExpr(leftBExpr=secondAndDoubleImpliesBExpr)
		#prepare the conclusion or
		orDoubleImpliesBExpr = OrBoolExpr.OrBoolExpr(leftBExpr=firstAndDoubleImpliesBExpr, rightBExpr=secondOrDoubleImpliesBExpr)
		result = orDoubleImpliesBExpr.evaluateProcess(state=state, listExecState=listExecState, typeExpr=ExpressionBaseElement.typeEvalBExpr, flagAtomic=False, flagReduction=True)
		assert type(result) is bool
		return result

	def evaluateNotReduction(self, state: Dict[str, float], listExecState: Union[List[str], None]) -> bool:
		#method to evaluate the boolean expression without Reduction -> use the minimum number of reductions
		isAtomic = self.typeId == self.typeOpNone
		leftValue = self.leftBoolExpr.evaluateProcess(state=state, listExecState=listExecState, typeExpr=ExpressionBaseElement.typeEvalBExpr, flagAtomic=isAtomic, flagReduction=False)
		assert type(leftValue) is bool
		#verify the case
		if self.typeId == self.typeOpNone:
			#the expression contains only a value -> execute it
			return leftValue
		elif self.typeId == self.typeOpImplies:
			#the expression is the implies operation
			#A->B : !A or B 
			result = not(leftValue) or self.rightBoolExpr.evaluateProcess(state=state, listExecState=listExecState, typeExpr=ExpressionBaseElement.typeEvalBExpr,  flagAtomic=False, flagReduction=False)
			assert type(result) is bool
			return result
		elif self.typeId == self.typeOpDoubleImplies:
			#A <-> B : (A and B) or (!A and !B)
			#it is necessary to compute the right value
			rightValue = self.rightBoolExpr.evaluateProcess(state=state, listExecState=listExecState, typeExpr=ExpressionBaseElement.typeEvalBExpr, flagAtomic=False, flagReduction=False)
			assert type(rightValue) is bool
			return (leftValue and rightValue) or (not(leftValue) and not(rightValue))
		else:
			raise Exception('typeId not valid')

	#########################################################################################################################################################
	#method for graphic 

	def toGraphicTree(self):
		#method to convert the boolean expression to a graphic tree
		from PyQt5 import QtWidgets
		if self.typeId == self.typeOpNone:
			graphicTree =  self.leftBoolExpr.toGraphicTree()
		else:
			graphicTree = QtWidgets.QTreeWidgetItem()
			graphicTree.setText(0, 'BExp')
			firstChildTree = self.leftBoolExpr.toGraphicTree()
			graphicTree.addChild(firstChildTree)
			#insert the second expression
			if self.typeId == self.typeOpImplies:
				opChild = QtWidgets.QTreeWidgetItem()
				opChild.setText(0, '->')
				graphicTree.addChild(opChild)
			elif self.typeId == self.typeOpDoubleImplies:
				opChild = QtWidgets.QTreeWidgetItem()
				opChild.setText(0, '<->')
				graphicTree.addChild(opChild)
			secondChildTree = self.rightBoolExpr.toGraphicTree()
			graphicTree.addChild(secondChildTree)
		return graphicTree



