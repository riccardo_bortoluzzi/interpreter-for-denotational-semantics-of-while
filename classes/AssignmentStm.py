#script to implement the class of assignament statement

from __future__ import annotations
from classes.ExpressionBaseElement import ExpressionBaseElement
from typing import Dict, List, Set, Union
from classes.StatementBaseElement import StatementBaseElement

from classes.Variable import Variable
from classes.ArithmExpr import ArithmExpr

class AssignmentStm(StatementBaseElement):
	var : Variable
	arithmExprAss : ArithmExpr

	def __init__(self, var:Variable, aExp : ArithmExpr) -> None:
		#method to inizialize the object
		super().__init__()
		self.var = var
		self.arithmExprAss = aExp

	def toStringTree(self, nTab=0) -> str:
		#method to convert the assignment expression to a string
		#start with the number of tabs
		baseTab = '\t'*nTab
		stringTree = baseTab + 'Ass[' + '\n'
		stringTree += self.var.toStringTree(nTab=nTab+1)
		stringTree += baseTab + '\t' + ' := ' + '\n'
		stringTree += self.arithmExprAss.toStringTree(nTab=nTab+1)
		stringTree += baseTab + ']' + '\n'
		return stringTree 

	def findVariableSet(self) -> Set[str]:
		variableSet = self.var.findVariableSet()
		variableSet.update(self.arithmExprAss.findVariableSet())
		return variableSet

	def execute(self, state: Dict[str, float], listShowWhile: Union[List[str], None], listExecState: Union[List[str], None], flagReduction: bool, variableSet: Set[str]) -> None:
		#metho to execute the statement of the variable : simply update the state
		#no differences between Reduction mode and not
		value = self.arithmExprAss.evaluateProcess(state=state, listExecState=listExecState, typeExpr=ExpressionBaseElement.typeEvalAExpr, flagAtomic=False, flagReduction=flagReduction)
		state[self.var.name] = value

	#########################################################################################################################################################
	#method for graphic 

	def toGraphicTree(self):
		#method to convert the assignment expression to a graphic tree
		from PyQt5 import QtWidgets
		graphicTree = QtWidgets.QTreeWidgetItem()
		graphicTree.setText(0, 'Ass')
		childTreeVar =  self.var.toGraphicTree()
		graphicTree.addChild(childTreeVar)
		opChild = QtWidgets.QTreeWidgetItem()
		opChild.setText(0, ':=')
		graphicTree.addChild(opChild)
		secondChildTree = self.arithmExprAss.toGraphicTree()
		graphicTree.addChild(secondChildTree)
		return graphicTree 
