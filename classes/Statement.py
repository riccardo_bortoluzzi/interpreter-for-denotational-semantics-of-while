#script to implement the class of statement
from __future__ import annotations
from classes.StatementBaseElement import StatementBaseElement
from typing import Dict, List, Set, Union
import classes.AtomStatement as AtomStatement


class Statement(StatementBaseElement):
	listStms : List[Union[AtomStatement.AtomStatement, Statement]] #list of statement

	def __init__(self, listStms : List[Union[AtomStatement.AtomStatement, Statement]]) -> None:
		#method to inizialize the object of a statement
		super().__init__()
		self.listStms = listStms

	def toStringTree(self, nTab=0) -> str:
		#method to convert the statement to a string
		#start with the number of tabs
		baseTab = '\t'*nTab
		#distinguish number of statement
		if len(self.listStms) == 1:
			stringTree = self.listStms[0].toStringTree(nTab=nTab)
		else:
			#more than 1 statement
			stringTree = baseTab + 'Stm[' + '\n'
			#insert the first statement
			stringTree += self.listStms[0].toStringTree(nTab=nTab+1)
			for i in range(1, len(self.listStms)):
				#insert the separator
				stringTree += baseTab + '\t' + ' ; ' + '\n'
				stringTree += self.listStms[i].toStringTree(nTab=nTab+1)
			#close the statement
			stringTree += baseTab + ']' + '\n'
		return stringTree

	def findVariableSet(self) -> Set[str]:
		variableSet = set()
		for stm in self.listStms:
			variableSetStm = stm.findVariableSet()
			assert variableSetStm is not None
			variableSet.update(variableSetStm)
		return variableSet

	def execute(self, state: Dict[str, float], listShowWhile: Union[List[str], None], listExecState: Union[List[str], None], flagReduction: bool, variableSet: Set[str]) -> None:
		#execute the statement in order
		isAtomic = len(self.listStms) == 1
		for stm in self.listStms:
			stm.executionProcess(state=state, listShowWhile=listShowWhile, listExecState=listExecState, flagReduction=flagReduction, variableSet=variableSet, flagAtomic=isAtomic)

	#########################################################################################################################################################
	#method for graphic 

	def toGraphicTree(self):
		#method to convert the statement to a graphic tree
		from PyQt5 import QtWidgets
		#number of statement
		if len(self.listStms) == 1:
			graphicTree = self.listStms[0].toGraphicTree() 
		else:
			#more than 1 statement
			graphicTree = QtWidgets.QTreeWidgetItem()
			graphicTree.setText(0, 'Stm')
			#insert the first statement
			graphicTree.addChild(self.listStms[0].toGraphicTree())
			for i in range(1, len(self.listStms)):
				#insert the separator
				sepChild = QtWidgets.QTreeWidgetItem()
				sepChild.setText(0, ';')
				graphicTree.addChild(sepChild)
				graphicTree.addChild(self.listStms[i].toGraphicTree())
		return graphicTree