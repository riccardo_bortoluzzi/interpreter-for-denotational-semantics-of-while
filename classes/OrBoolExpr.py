#script to implement the binary or boolean expression

from __future__ import annotations
import classes.AndBoolExpr as AndBoolExpr
import classes.BoolExpr as BoolExpr
import classes.AtomBoolExpr as AtomBoolExpr
from typing import Dict, List, Set, Union
from  classes.ExpressionBaseElement import ExpressionBaseElement



class OrBoolExpr(ExpressionBaseElement):
	leftBoolExpr : AndBoolExpr.AndBoolExpr
	rightBoolExpr : Union[None, OrBoolExpr]

	def __init__(self, leftBExpr:AndBoolExpr.AndBoolExpr, rightBExpr:Union[None, OrBoolExpr] = None) -> None:
		#constructor method that simply populate attributes
		super().__init__()
		self.leftBoolExpr = leftBExpr
		self.rightBoolExpr = rightBExpr

	def toStringTree(self, nTab=0) -> str:
		#method to convert the boolean expression to a string
		#start with the number of tabs
		baseTab = '\t'*nTab
		#verify the length of the expression
		if self.rightBoolExpr is None:
			#only 1 expression -> not increment indent
			stringTree = self.leftBoolExpr.toStringTree(nTab=nTab)
		else:
			#more tha 1 expression
			stringTree = baseTab + 'BExp[' + '\n'
			stringTree += self.leftBoolExpr.toStringTree(nTab=nTab+1)
			stringTree += baseTab + '\t' +' || ' + '\n'
			stringTree += self.rightBoolExpr.toStringTree(nTab=nTab+1)
			stringTree += baseTab + ']' + '\n'
		return stringTree

	def findVariableSet(self) -> Set[str]:
		variableSet = self.leftBoolExpr.findVariableSet()
		if self.rightBoolExpr is not None:
			variableSet.update(self.rightBoolExpr.findVariableSet())
		return variableSet

	def evaluate(self, state: Dict[str, float], listExecState: Union[List[str], None], flagReduction: bool) -> bool:
		#method to evaluate the or operator
		#distinguish case
		if flagReduction : 
			return self.evaluateReduction(state=state, listExecState=listExecState)
		else:
			return self.evaluateNotReduction(state=state, listExecState=listExecState)
	
	def evaluateReduction(self, state: Dict[str, float], listExecState: Union[List[str], None]) -> bool:
		#to evaluate the real or operator in Reduction mode, it is used the De Morgan's law of or: (A or B) = not( not(A) and not(B) )
		if self.rightBoolExpr is None:
			#evaluate only the first expression
			return bool(self.leftBoolExpr.evaluateProcess(state=state, listExecState=listExecState, typeExpr=ExpressionBaseElement.typeEvalBExpr, flagAtomic=True, flagReduction=True))
		#there is also the second expression
		#the or statement is transformed into an equivalent and statement
		#first calculate the object of OrBoolExpr
		leftOrBoolExpr = OrBoolExpr(leftBExpr=self.leftBoolExpr)
		#then create the object of BoolExpr
		leftBoolExpr = BoolExpr.BoolExpr(leftBExpr=leftOrBoolExpr)
		#create the atom expression with parenthesis
		leftParBoolExpr = AtomBoolExpr.AtomBoolExpr(par=leftBoolExpr)
		#create the object of not of the left
		leftNotBoolExpr = AtomBoolExpr.AtomBoolExpr(par=leftParBoolExpr)
		#now create the not object for the right side
		#create the object of BoolExpr
		rightBoolExpr = BoolExpr.BoolExpr(leftBExpr=self.rightBoolExpr)
		#create the atom expression with parenthesis
		rightParBoolExpr = AtomBoolExpr.AtomBoolExpr(par=rightBoolExpr)
		#create the object of not of the right
		rightNotBoolExpr = AtomBoolExpr.AtomBoolExpr(par=rightParBoolExpr)
		#create the And object of the right
		rightAndBoolExpr = AndBoolExpr.AndBoolExpr(leftBExpr=rightNotBoolExpr)
		#create the And boolean expression
		andInnerNotBoolExpr = AndBoolExpr.AndBoolExpr(leftBExpr=leftNotBoolExpr, rightBExpr=rightAndBoolExpr)
		#it is necessary to negate this statement
		orInnerNotBoolExpr = OrBoolExpr(leftBExpr=andInnerNotBoolExpr)
		innerNotBExpr = BoolExpr.BoolExpr(leftBExpr=orInnerNotBoolExpr)
		parInnerNotBExpr = AtomBoolExpr.AtomBoolExpr(innerNotBExpr)
		equivalentOrBExpr = AtomBoolExpr.AtomBoolExpr(parInnerNotBExpr)
		#evaluate the and equivalent
		return bool(equivalentOrBExpr.evaluateProcess(state=state, listExecState=listExecState, typeExpr=ExpressionBaseElement.typeEvalBExpr, flagAtomic=False, flagReduction=True))

	def evaluateNotReduction(self, state: Dict[str, float], listExecState: Union[List[str], None]) -> bool:
		#to evaluate the real or operator without Reduction mode, 
		isAtomic = self.rightBoolExpr is None
		#evaluate the first expression
		leftValue = bool(self.leftBoolExpr.evaluateProcess(state=state, listExecState=listExecState, typeExpr=ExpressionBaseElement.typeEvalBExpr, flagAtomic=isAtomic, flagReduction=False))
		#verify if it is necessary to evaluate also the second
		if isAtomic or leftValue: #left value true
			#can return the first value
			return leftValue
		#need to evaluate the second 
		assert self.rightBoolExpr is not None
		rightValue = bool(self.rightBoolExpr.evaluateProcess(state=state, listExecState=listExecState, typeExpr=ExpressionBaseElement.typeEvalBExpr, flagAtomic=False, flagReduction=False))
		#return only the second for short circuit
		return rightValue

	#########################################################################################################################################################
	#method for graphic 

	def toGraphicTree(self):
		#method to convert the boolean expression to a graphic tree
		from PyQt5 import QtWidgets
		#verify the length of the expression
		if self.rightBoolExpr is None:
			#only 1 expression -> not increment indent
			graphicTree = self.leftBoolExpr.toGraphicTree()
		else:
			#more tha 1 expression
			graphicTree = QtWidgets.QTreeWidgetItem()
			graphicTree.setText(0, 'BExp')
			firstChilTree = self.leftBoolExpr.toGraphicTree()
			graphicTree.addChild(firstChilTree)
			opChild = QtWidgets.QTreeWidgetItem()
			opChild.setText(0, '||')
			graphicTree.addChild(opChild)
			secondChildTree = self.rightBoolExpr.toGraphicTree()
			graphicTree.addChild(secondChildTree)
		return graphicTree 

