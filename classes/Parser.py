#script to implement the parser 

from classes.OrBoolExpr import OrBoolExpr
from classes.AndBoolExpr import AndBoolExpr
from classes.ForStm import ForStm
from classes.RepeatStm import RepeatStm
from classes.WhileStm import WhileStm
from classes.IfStm import IfStm
from classes.SkipStm import SkipStm
from classes.AssignmentStm import AssignmentStm
from classes.Statement import Statement
from classes.AtomStatement import AtomStatement
from classes.BoolExpr import BoolExpr
from classes.AtomBoolExpr import AtomBoolExpr
from classes.AtomArithmExpr import AtomArithmExpr
from classes.ArithmBoolExpr import ArithmBoolExpr
from classes.ArithmExpr import ArithmExpr
from classes.Variable import Variable

import re
from typing import List, Tuple, Union

class Parser:

	def __init__(self) -> None:
		pass

	def parseString(self, pattern:str, stringToAnalyze:str) -> Tuple[str, str]:
		#method to parse a string and returns a tuple with the string parsed without white characters and what remains to the parse
		#stringPatternComplete = r'(\s*'+ re.compile(pattern) + r'\s*)' #I don't use the begin of the string ^ because it is included in match
		stringPatternComplete = r'(\s*'+ pattern + r'\s*)' #I don't use the begin of the string ^ because it is included in match
		matchObject = re.match(stringPatternComplete, stringToAnalyze)
		if matchObject is None:
			#the string was not found
			return ('', stringToAnalyze)
		else:
			#the pattern was found
			remainString = stringToAnalyze[ matchObject.end(): ]
			parseString = re.sub(pattern=r'\s', repl='', string=matchObject.group())
			return (parseString, remainString)

	def parseKeyWord(self, keyWord:str, stringToAnalyze:str) -> Tuple[str, str]:
		#method to parse a keyword and verify that the keyword is not a part of a variable name
		pattern = keyWord + r'(?![a-zA-Z_0-9])'
		#pattern = keyWord + r'(?=$|\s|;|\)|&)'
		return self.parseString(pattern=pattern, stringToAnalyze=stringToAnalyze)

	def parseVariable(self, string:str) -> Union[Tuple[Variable, str] , Tuple[None, str]]:
		#method to search for a variable name
		#variable starts with a letter and contains upper and lower characters, numbers and underscore
		patternVariable = r'[a-zA-Z][a-z0-9A-Z_]*'
		(name, remain) = self.parseString(pattern=patternVariable, stringToAnalyze=string)
		if name == '' or (name in ('true', 'false', 'skip', 'if', 'then', 'else', 'while', 'do', 'repeat', 'until', 'for', 'to', 'end')):
			#variable not found or it has a protected name
			return (None, string)
		else:
			newVariable = Variable(name=name)
			return (newVariable, remain)

	def parseNumerals(self, string:str) -> Union[Tuple[float, str], Tuple[None, str]]:
		#method to search for numerals
		#numerals are also float, the separator is the point (.)
		patternNumerals = r'\d*\.?\d*'
		(strValue, remain) = self.parseString(pattern=patternNumerals, stringToAnalyze=string)
		if strValue == '':
			#numeral not found
			return (None, string)
		else:
			#numeral found
			numeral = float(strValue)
			return (numeral, remain)

	##############################################################################################################################################################################
	#arithmetic expression

	def parseAtomArithmExpr(self, string:str) -> Union[Tuple[AtomArithmExpr, str], Tuple[None, str]]:
		#method to parse a single atomic arithmetic expression
		#check if it is a numerals
		(numeral, remain) = self.parseNumerals(string=string)
		if numeral is not None:
			#numeral found
			nextAtomAExpr = AtomArithmExpr(par=numeral)
			return (nextAtomAExpr, remain)
		#check if it is a variable
		(var, remain) = self.parseVariable(string=string)
		if var is not None:
			#variable found
			nextAtomAExpr = AtomArithmExpr(par=var)
			return (nextAtomAExpr, remain)
		#check if it contains parenthesis
		(firstPar, remainFirstPar) = self.parseString(pattern=r'\(', stringToAnalyze=string)
		if firstPar != '':
			#first parenthesis found
			(innerArithmExpr, remainAfterIAExpr) = self.parseArithmExpr(string=remainFirstPar)
			#control if the expression was found
			if innerArithmExpr is not None:
				#now it controls for the second parenthesis
				(secondPar, remainSecondPar) = self.parseString(pattern=r'\)', stringToAnalyze=remainAfterIAExpr)
				if secondPar != '':
					#second parenthesis found
					nextAtomAExpr = AtomArithmExpr(par=innerArithmExpr)
					return (nextAtomAExpr, remainSecondPar)
				else:
					#second parenthesis was not found -> syntax error (verify)
					raise Exception('Syntax error: ' + remainSecondPar)
		#at the end atomic arithmetic expression not found
		return (None, string)

	def parseArithmExpr(self, string:str) -> Union[Tuple[ArithmExpr, str], Tuple[None, str]]:
		#method to parse a complete arithmetic expression
		#search for an atomic arithmetic expression
		(firstArithmExpr, remainAfterFirstStep) = self.parseAtomArithmExpr(string=string)
		#check if expression found
		if firstArithmExpr is not None:
			patternOp = r'(?:\+|\*|-(?!>))'#r'[+*-]' #pattern to find operation +, *, - and excludes ->
			listExpr :List[AtomArithmExpr] = [firstArithmExpr] #list that contains the arithmetic expression in order
			listOp :List[str] = [] #list that contains the operations to join arithmetic expression
			#search for an operation
			(nextOp, remainString) = self.parseString(pattern=patternOp, stringToAnalyze=remainAfterFirstStep)
			while nextOp != '':
				#while an operation was found
				listOp.append(nextOp)
				#I'm expecting another atomic expression
				(nextArithmExpr, remainString) = self.parseAtomArithmExpr(string=remainString)
				if nextArithmExpr is None:
					#arithmetic expression not found -> exception
					raise Exception('Syntax error: ' + remainString)
				listExpr.append(nextArithmExpr)
				#search for another operation
				(nextOp, remainString) = self.parseString(pattern=patternOp, stringToAnalyze=remainString)
			#calculate the new arithmetic expression
			newArithmeticExpr = ArithmExpr(listAtomAExpr=listExpr,listOp=listOp)
			return (newArithmeticExpr, remainString)
		#if nothing was found, returns the None tuple
		return (None, string)

	###############################################################################################################################################################################
	#boolean expression

	def parseArithmBoolExpr(self, string:str) -> Union[Tuple[ArithmBoolExpr, str], Tuple[None, str]]:
		#method to parse the arithmetic boolean expression
		#search for first expression
		(firstArithmExpr, remainFirstAExpr) = self.parseArithmExpr(string=string)
		if firstArithmExpr is not None:
			#search for the symbol
			patternOp = r'(?:[=|]=|(?<!-)>=?|<=?(?!-))'#r'[=≠<≤>≥]'
			(strOp, remainAfterOp) = self.parseString(pattern=patternOp, stringToAnalyze=remainFirstAExpr)
			#if the op was found
			if strOp != '':
				#search for the second arithmetic expression
				(secondArithmExpr, remainSecondAExpr) = self.parseArithmExpr(string=remainAfterOp)
				if secondArithmExpr is not None:
					newArithmBoolExpr = ArithmBoolExpr(leftAExpr=firstArithmExpr,strOp=strOp, rightAExpr=secondArithmExpr)
					return (newArithmBoolExpr, remainSecondAExpr)
				else:
					#second arithmetic expression is None -> syntax erorr
					raise Exception('Syntax error: ' + remainSecondAExpr)
		#at the end the arithmetic boolean expression was not found
		return (None, string)

	def parseAtomBoolExpr(self, string:str) -> Union[Tuple[AtomBoolExpr, str], Tuple[None, str]]:
		#method to parse an atomic boolean expression
		#search for single value
		(strTrue, remainAfterFirst) = self.parseKeyWord(keyWord=r'true', stringToAnalyze=string) 
		if strTrue != '':
			#true value found
			newAtomBoolExpr = AtomBoolExpr(par=True)
			return (newAtomBoolExpr, remainAfterFirst)
		#true not found
		(strFalse, remainAfterFirst) = self.parseKeyWord(keyWord=r'false', stringToAnalyze=string)
		if strFalse != '':
			#false value found
			newAtomBoolExpr = AtomBoolExpr(par=False)
			return (newAtomBoolExpr, remainAfterFirst)
		#search for a not expression
		(strNot, remainAfterFirst) = self.parseString(pattern='!', stringToAnalyze=string)
		if strNot != '':
			#search for the next atomic boolean expression
			(notAtomBExpr, remainAfterSecond) = self.parseAtomBoolExpr(string=remainAfterFirst)
			if notAtomBExpr is None:
				#atomic expression not found -> syntax error
				raise Exception('Syntax error: ' + remainAfterSecond)
			#return the expression with not
			newAtomBoolExpr = AtomBoolExpr(par=notAtomBExpr)
			return (newAtomBoolExpr, remainAfterSecond)
		#search for parenthesis
		(strFirstPar, remainAfterFirst) = self.parseString(pattern=r'\(', stringToAnalyze=string)
		if strFirstPar != '':
			#first parenthesis found
			#search for boolean expression
			(innerBoolExpr, remainAfterSecond) = self.parseBoolExpr(string=remainAfterFirst)
			if innerBoolExpr is not None:
				#search for the second parenthesis
				(strSecondPar, remainAfterThird) = self.parseString(pattern=r'\)', stringToAnalyze=remainAfterSecond)
				if strSecondPar == '':
					#second parenthesis was not found -> syntax error
					raise Exception('Syntax error: ' + remainAfterThird)
				else:
					#second parenthesis was found
					newAtomBoolExpr = AtomBoolExpr(par=innerBoolExpr)
					return (newAtomBoolExpr, remainAfterThird)
		#search for arithmetic expression
		(arithmBoolExpr, remainAfterFirst) = self.parseArithmBoolExpr(string=string)
		if arithmBoolExpr is not None:
			#arithmetic expression
			newAtomBoolExpr = AtomBoolExpr(par=arithmBoolExpr)
			return (newAtomBoolExpr, remainAfterFirst)
		#at the end returns the None tuple
		return (None, string)

	def parseAndBoolExpr(self, string:str) -> Union[Tuple[AndBoolExpr, str], Tuple[None, str]]:
		#method to parse an And bool expression
		#search for first atomic boolean expression
		(leftAtomBoolExpr, remainAfterFirst) = self.parseAtomBoolExpr(string=string)
		if leftAtomBoolExpr is not None:
			#atomic boolean expression was found
			patternOp = r'&&'# #pattern to find && (and)
			(nextOp, remainString) = self.parseString(pattern=patternOp, stringToAnalyze=remainAfterFirst)
			#if the operation was found search for the second expression
			if nextOp != '':
				(rightAndBoolExpression, remainString) = self.parseAndBoolExpr(string=remainString)
				if rightAndBoolExpression is None:
					#atomic boolean expression not found -> exception
					raise Exception('Syntax error: ' + remainString)
				newAndBoolExpr = AndBoolExpr(leftBExpr=leftAtomBoolExpr, rightBExpr=rightAndBoolExpression)
			else:
				#operator not found
				newAndBoolExpr = AndBoolExpr(leftBExpr=leftAtomBoolExpr)
			return (newAndBoolExpr, remainString)
		#not found first boolean expression -> return the None tuple
		return (None, string)

	def parseOrBoolExpr(self, string:str) -> Union[Tuple[OrBoolExpr, str], Tuple[None, str]]:
		#method to parse an Or bool expression
		#search for first and boolean expression
		(leftAtomBoolExpr, remainAfterFirst) = self.parseAndBoolExpr(string=string)
		if leftAtomBoolExpr is not None:
			#and boolean expression was found
			patternOp = r'\|\|'# #pattern to find || (or)
			(nextOp, remainString) = self.parseString(pattern=patternOp, stringToAnalyze=remainAfterFirst)
			#if the operation was found search for the second expression
			if nextOp != '':
				(rightAndBoolExpression, remainString) = self.parseOrBoolExpr(string=remainString)
				if rightAndBoolExpression is None:
					#atomic boolean expression not found -> exception
					raise Exception('Syntax error: ' + remainString)
				newOrBoolExpr = OrBoolExpr(leftBExpr=leftAtomBoolExpr, rightBExpr=rightAndBoolExpression)
			else:
				#operator not found
				newOrBoolExpr = OrBoolExpr(leftBExpr=leftAtomBoolExpr)
			return (newOrBoolExpr, remainString)
		#not found first boolean expression -> return the None tuple
		return (None, string)

	def parseBoolExpr(self, string:str) -> Union[Tuple[BoolExpr, str], Tuple[None, str]]:
		#method to parse a complete boolean expression
		#search for first andOrBoolExpr
		(leftOrBExpr, remainAfterFirst) = self.parseOrBoolExpr(string=string)
		if leftOrBExpr is not None:
			#atomic boolean expression was found
			patternOp = r'(?:<->|->)'# #pattern to find operation -> (implies) or <-> (double implies)
			(strOp, remainString) = self.parseString(pattern=patternOp, stringToAnalyze=remainAfterFirst)
			if strOp != '':
				#the operation was found
				#I'm expecting another BoolExc boolean expression
				(rightAndOrBExpr, remainString) = self.parseBoolExpr(string=remainString)
				if rightAndOrBExpr is None:
					#andOr boolean expression not found -> exception
					raise Exception('Syntax error: ' + remainString)
				newBoolExpr = BoolExpr(leftBExpr=leftOrBExpr, strOp=strOp, rightBExpr=rightAndOrBExpr)
			else:
				#tyhe operator was not found
				newBoolExpr = BoolExpr(leftBExpr=leftOrBExpr, strOp=strOp)
			return (newBoolExpr, remainString)
		#not found first boolean expression -> return the None tuple
		return (None, string)

	########################################################################################################################################################################################
	#methods to parse statements

	def parseAssignmentStatement(self, string:str) -> Union[Tuple[AssignmentStm, str], Tuple[None, str]]:
		#method to parse an assignment
		#search for variable
		(varAss, remainAfterFirst) = self.parseVariable(string=string)
		if varAss is not None:
			#variable found, search for assignment symbol
			(strAss, remainAfterSecond) = self.parseString(pattern=r':=', stringToAnalyze=remainAfterFirst)
			if strAss != '':
				#assignment operator found
				#search for arithmetic expression
				(arithmExprAss, remainAfterThird) = self.parseArithmExpr(string=remainAfterSecond)
				#if arithm expression was not found -> syntax error
				if arithmExprAss is None:
					raise Exception('Syntax error: ' + remainAfterThird)
				#create the new object of assignmenet
				newAssignment = AssignmentStm(var=varAss, aExp=arithmExprAss)
				return (newAssignment, remainAfterThird)
		#at the end return the None tuple
		return (None, string)

	def parseSkipStatement(self, string:str) -> Union[Tuple[SkipStm, str], Tuple[None, str]]:
		#method to parse the skip statement
		#search for skip keyword
		(skipStr, remainAfterFirst) = self.parseKeyWord(keyWord=r'skip', stringToAnalyze=string)
		if skipStr != '':
			#skip keyword found
			newSkip = SkipStm()
			return (newSkip, remainAfterFirst)
		#skip not fount -> None Tuple
		return (None, string)

	def parseDoStatement(self, string:str) -> Union[Tuple[Statement, str], Tuple[None, str]]:
		#method to parse a statemente that starts with do (for the while and for costruction)
		#search for do string
		(doStr, remainAfterFirst) = self.parseKeyWord(keyWord=r'do', stringToAnalyze=string)
		if doStr != '':
			#the do keyword found
			(innerStatement, remainAfterSecond) = self.parseStatement(string=remainAfterFirst)
			#if the statement was not found -> syntax error
			if innerStatement is None:
				raise Exception('Syntax error: ' + remainAfterSecond)
			#the inner statement was found
			#search for the endstm keyword
			(endstmStr, remainAfterThird) = self.parseKeyWord(keyWord=r'end', stringToAnalyze=remainAfterSecond)
			if endstmStr == '':
				#keyword not found
				raise Exception('Syntax error: ' + remainAfterThird)
			return (innerStatement, remainAfterThird)
		#at the end return the None tuple
		return (None, string)

	def parseIfStatement(self, string:str) -> Union[Tuple[IfStm, str], Tuple[None, str]]:
		#method to parse the if statement
		#search for the if keyword
		(strIf, remainAfterIf) = self.parseKeyWord(keyWord=r'if', stringToAnalyze=string)
		if strIf != '':
			#keyword found
			(guardBoolExpr, remainAfterGuard) = self.parseBoolExpr(string=remainAfterIf)
			#if guard not found -> syntax error
			if guardBoolExpr is None:
				raise Exception('Syntax error: ' + remainAfterGuard)
			#search for then keyword
			(strThen, remainAfterStrThen) = self.parseKeyWord(keyWord=r'then', stringToAnalyze=remainAfterGuard)
			#if then not found -> syntax error
			if strThen == '':
				raise Exception('Syntax error: ' + remainAfterStrThen)
			#then keyword found
			#search for first statement
			(thenStatement, remainAfterThen) = self.parseStatement(string=remainAfterStrThen)
			#if the statement not found -> syntax error
			if thenStatement is None:
				raise Exception('Syntax error: ' + remainAfterThen)
			#search for else keyword
			(strElse, remainAfterStrElse) = self.parseKeyWord(keyWord=r'else', stringToAnalyze=remainAfterThen)
			if strElse == '':
				raise Exception('Syntax error: ' + remainAfterStrElse)
			(elseStatement, remainAfterElse) = self.parseStatement(string=remainAfterStrElse)
			if elseStatement is None:
				#statement not found -> syntax error
				raise Exception('Syntax error: ' + remainAfterElse)
			#search for the endstm keyword
			(endstmStr, remainAfterEndstm) = self.parseKeyWord(keyWord=r'end', stringToAnalyze=remainAfterElse)
			if endstmStr == '':
				#keyword not found
				raise Exception('Syntax error: ' + remainAfterEndstm)
			#create the new object 
			newIfStatement = IfStm(guard=guardBoolExpr, stmThen=thenStatement, stmElse=elseStatement)
			return (newIfStatement, remainAfterEndstm)
		#at the end return the None tuple
		return (None, string)

	def parseWhileStatement(self, string:str) -> Union[Tuple[WhileStm, str], Tuple[None, str]]:
		#method to parse the while statement
		#search for while keyword
		(strWhile, remainAfterWhile) = self.parseKeyWord(keyWord=r'while', stringToAnalyze=string)
		if strWhile != '':
			#string while found
			(guardBoolExpr, remainAfterGuard) = self.parseBoolExpr(string=remainAfterWhile)
			if guardBoolExpr is None:
				#guard not found -> syntax error
				raise Exception('Syntax error: ' + remainAfterGuard)
			#search fo the statement with do
			(stmWhile, remainAfterDo) = self.parseDoStatement(string=remainAfterGuard)
			if stmWhile is None:
				#syntax error because the statement of while not found
				raise Exception('Syntax error: '+ remainAfterDo)
			#create the new object
			newWhileStatement = WhileStm(guard=guardBoolExpr, stm=stmWhile)
			return (newWhileStatement, remainAfterDo)
		#at the end return the None tuple
		return (None, string)

	def parseRepeatStatement(self, string:str) -> Union[Tuple[RepeatStm, str], Tuple[None, str]]:
		#method to parse the repeat until statement
		#search for repeat keyword
		(strRepeat, remainAfterRepeat) = self.parseKeyWord(keyWord=r'repeat', stringToAnalyze=string)
		if strRepeat != '':
			#string repeat found
			#search fo the statement
			(stmRepeat, remainAfterStm) = self.parseStatement(string=remainAfterRepeat)
			if stmRepeat is None:
				#syntax error because the statement of repeat not found
				raise Exception('Syntax error: '+ remainAfterStm)
			#search for until keyword
			(strUntil, remainAfterUntil) = self.parseKeyWord(keyWord=r'until', stringToAnalyze=remainAfterStm)
			if strUntil == '':
				#string until not found -> syntax error
				raise Exception('Syntax error: ' + remainAfterUntil)
			#search for the guard 
			(guardBoolExpr, remainAfterGuard) = self.parseBoolExpr(string=remainAfterUntil)
			if guardBoolExpr is None:
				#guard not found -> syntax error
				raise Exception('Syntax error: ' + remainAfterGuard)
			#create the new object
			newRepeatStatement = RepeatStm(stm=stmRepeat, guard=guardBoolExpr)
			return (newRepeatStatement, remainAfterGuard)
		#at the end return the None tuple
		return (None, string)

	def parseForStatement(self, string:str) -> Union[Tuple[ForStm, str], Tuple[None, str]]:
		#method to parse a for statement
		#search for the for keyword
		(strFor, remainAfterFor) = self.parseKeyWord(keyWord=r'for', stringToAnalyze=string)
		if strFor != '':
			#search for the first inizialization
			(startAssignStm, remainAfterAssign) = self.parseAssignmentStatement(string=remainAfterFor)
			if startAssignStm is None:
				#synatx error
				raise Exception('Syntax error: ' + remainAfterAssign)
			#search for to keyword
			(strTo, remainAfterTo) = self.parseKeyWord(keyWord=r'to', stringToAnalyze=remainAfterAssign)
			if strTo == '':
				#keyword not found -> syntax error
				raise Exception('Syntax error: ' + remainAfterTo)
			#search for the second arithmetic expression
			(endArithmExpr, remainAfterEndAExpr) = self.parseArithmExpr(string=remainAfterTo)
			if endArithmExpr is None:
				#expression not found -> syntax error
				raise Exception('Syntax error:' + remainAfterEndAExpr)
			#search the statement with do
			(stmDo, remainAfterStm) = self.parseDoStatement(string=remainAfterEndAExpr)
			if stmDo is None:
				#syntax error because the statement was not found
				raise Exception('Synatx error: ' + remainAfterStm)
			#create the new object
			newForStatement = ForStm(assStart=startAssignStm, aExprEnd=endArithmExpr, stm=stmDo)
			return (newForStatement, remainAfterStm)
		#at the end return the None tuple
		return (None, string)

	def parseAtomStatement(self, string:str) -> Union[Tuple[AtomStatement, str], Tuple[None, str]]:
		#method to parse a single statement
		#search skip statement
		(atomStatement, remain) = self.parseSkipStatement(string=string)
		if atomStatement is not None:
			#skip found
			newAtomStatement = AtomStatement(par=atomStatement)
			return (newAtomStatement, remain)
		#search assignment statement
		(atomStatement, remain) = self.parseAssignmentStatement(string=string)
		if atomStatement is not None:
			#assignment found
			newAtomStatement = AtomStatement(par=atomStatement)
			return (newAtomStatement, remain)
		#search if statement
		(atomStatement, remain) = self.parseIfStatement(string=string)
		if atomStatement is not None:
			#if statement found
			newAtomStatement = AtomStatement(par=atomStatement)
			return (newAtomStatement, remain)
		#search while statement
		(atomStatement, remain) = self.parseWhileStatement(string=string)
		if atomStatement is not None:
			#while statement found
			newAtomStatement = AtomStatement(par=atomStatement)
			return (newAtomStatement, remain)
		#search repeat until statement
		(atomStatement, remain) = self.parseRepeatStatement(string=string)
		if atomStatement is not None:
			#repeat until found
			newAtomStatement = AtomStatement(par=atomStatement)
			return (newAtomStatement, remain)
		#search for statement
		(atomStatement, remain) = self.parseForStatement(string=string)
		if atomStatement is not None:
			#for statement found
			newAtomStatement = AtomStatement(par=atomStatement)
			return (newAtomStatement, remain)
		#at the end return the None tuple
		return (None, string)

	def parseStatement(self, string:str) -> Union[Tuple[Statement, str], Tuple[None, str]]:
		#method to parse an entire statement
		#search for first atomic statement
		(firstStatement, remainAfterFirst) = self.parseAtomStatement(string=string)
		if firstStatement is not None:
			#first statement found
			listStatements:List[Union[AtomStatement, Statement]] = [firstStatement]
			#search for a ;
			(strSepStatement, remain) = self.parseString(pattern=r';', stringToAnalyze=remainAfterFirst)
			while strSepStatement != '':
				#separator for statement found -> search for next statement
				(nextStatement, remain) = self.parseStatement(string=remain)
				#another statement is expected
				if nextStatement is None:
					#syntax error
					raise Exception('Syntax error: ' + remain)
				listStatements.append(nextStatement)
				#search fo another ;
				(strSepStatement, remain) = self.parseString(pattern=r';', stringToAnalyze=remain)
			#at the end create the new statement object
			newStatement = Statement(listStms=listStatements)
			return (newStatement, remain)
		#statement not found
		return (None, string)
