#script to implement the for statement

from __future__ import annotations
from classes.BoolExpr import BoolExpr
from classes.OrBoolExpr import OrBoolExpr
from classes.AtomBoolExpr import AtomBoolExpr
from classes.ArithmBoolExpr import ArithmBoolExpr
from classes.AndBoolExpr import AndBoolExpr
from classes.AtomArithmExpr import AtomArithmExpr
from classes.ExpressionBaseElement import ExpressionBaseElement
from typing import Dict, List, Set, Union
from classes.StatementBaseElement import StatementBaseElement
from classes.ArithmExpr import ArithmExpr
from classes.Variable import Variable
import classes.Statement as Statement
import classes.AssignmentStm as AssignmentStm
import classes.AtomStatement as AtomStatement
import classes.WhileStm as WhileStm

class ForStm(StatementBaseElement):
	var : Variable
	arithmExprStart : ArithmExpr
	arithmExprEnd : ArithmExpr
	stm : Statement.Statement
	numberOfExecution : int #number of execution the for


	def __init__(self, assStart : AssignmentStm.AssignmentStm, aExprEnd : ArithmExpr, stm:Statement.Statement) -> None:
		#method to inizialize attribute, usign assignment to compute the variable and the start expression
		super().__init__()
		self.var = assStart.var
		self.arithmExprStart = assStart.arithmExprAss
		self.arithmExprEnd = aExprEnd
		self.stm = stm

	def toStringTree(self, nTab=0) -> str:
		#method to convert the for statement to a string
		#start with the number of tabs
		baseTab = '\t'*nTab
		stringTree = baseTab + 'for ' + '\n'
		stringTree += self.var.toStringTree(nTab=nTab+1)
		stringTree += baseTab + ' from ' + '\n'
		stringTree += self.arithmExprStart.toStringTree(nTab=nTab+1)
		stringTree += baseTab + ' to ' + '\n'
		stringTree += self.arithmExprEnd.toStringTree(nTab=nTab+1)
		stringTree += baseTab + ' do ' + '\n'
		stringTree += self.stm.toStringTree(nTab=nTab+1)
		return stringTree

	def findVariableSet(self) -> Set[str]:
		variableSet = self.var.findVariableSet()
		variableSet.update(self.arithmExprStart.findVariableSet())
		variableSet.update(self.arithmExprEnd.findVariableSet())
		variableSet.update(self.stm.findVariableSet())
		return variableSet

	def execute(self, state: Dict[str, float], listShowWhile: Union[List[str], None], listExecState: Union[List[str], None], flagReduction: bool, variableSet: Set[str]) -> None:
		#distinguish if the falg Reduction is selected
		if flagReduction:
			self.executeReduction(state=state, listShowWhile=listShowWhile, listExecState=listExecState, variableSet=variableSet)
		else:
			self.executeNotReduction(state=state, listShowWhile=listShowWhile, listExecState=listExecState, variableSet=variableSet)

	def executeReduction(self, state: Dict[str, float], listShowWhile: Union[List[str], None], listExecState: Union[List[str], None], variableSet: Set[str]) -> None:
		#the execution of a for statement is transformed into a while statement with a temporary variable
		#for x:= A1 to A2 do S
		#->
		#tmpVar := A1
		#while tmpVar < A2 do
		#	x := tmpVar;
		#	S;
		#	tmpVar := tmpVar +1
		#first create the temporary variable
		tmpVariableName = self.calcNewVariableName(variableSet=variableSet)
		#add the name to variable set to prevent overwritten of the variable
		variableSet.add(tmpVariableName)
		tmpVariable = Variable(name=tmpVariableName)
		#execute the start assignacion
		startAssVar = AssignmentStm.AssignmentStm(var=self.var, aExp=self.arithmExprStart)
		startAssVar.executionProcess(state=state, listShowWhile=listShowWhile, listExecState=listExecState, flagReduction=True, variableSet=variableSet, flagAtomic=False)
		#now recall the same value
		startValue = self.var.evaluate(state=state, listExecState=listExecState, flagReduction=True)
		#create the expression for the initial value
		atomStartValueAExpr = AtomArithmExpr(startValue)
		startValueAExpr = ArithmExpr(listAtomAExpr=[atomStartValueAExpr])
		#create the assignement of the temporary variable whit the log
		stratAssignacionTmpVar = AssignmentStm.AssignmentStm(var=tmpVariable, aExp=startValueAExpr)
		stratAssignacionTmpVar.executionProcess(state=state, listShowWhile=listShowWhile, listExecState=listExecState, flagReduction=True, variableSet=variableSet, flagAtomic=False)
		#create the statement for the while
		#create the arithmetic expression of the temporary variable
		atomTmpVarAExpr = AtomArithmExpr(tmpVariable)
		tmpVariableAExpr = ArithmExpr([atomTmpVarAExpr])
		#create the assignement of the for variable
		firstAssForVar = AssignmentStm.AssignmentStm(var=self.var, aExp=tmpVariableAExpr) #x:= tmpVar
		atomAssForVar = AtomStatement.AtomStatement(firstAssForVar)
		#create the final increment of the temporary variable
		atomOneAExpr = AtomArithmExpr(1.)
		incrementAExpr = ArithmExpr(listAtomAExpr=[tmpVariableAExpr, atomOneAExpr], listOp=['+'])
		assIncrementTmpVar = AssignmentStm.AssignmentStm(var=tmpVariable, aExp=incrementAExpr)
		atomAssIncTmpVar = AtomStatement.AtomStatement(assIncrementTmpVar)
		listStatementWhile = [atomAssForVar] + self.stm.listStms[:] + [atomAssIncTmpVar]
		#create the statement for the while
		newStmOfWhile = Statement.Statement(listStatementWhile)
		#create the booalean guard for the while
		#evaluate the final value
		finalValue = self.arithmExprEnd.evaluateProcess(state=state, listExecState=listExecState, typeExpr=ExpressionBaseElement.typeEvalAExpr, flagReduction=True,flagAtomic=False)
		atomFinalValueAExpr = AtomArithmExpr(finalValue)
		finalValueAExpr = ArithmExpr([atomFinalValueAExpr])
		whileGuardABExpr = ArithmBoolExpr(leftAExpr=tmpVariableAExpr, strOp=ArithmBoolExpr.strLT, rightAExpr=finalValueAExpr)
		atomWhileGuardBExpr = AtomBoolExpr(whileGuardABExpr)
		andWhileGuardBExpr = AndBoolExpr(atomWhileGuardBExpr)
		orWhileGuardBExpr = OrBoolExpr(leftBExpr=andWhileGuardBExpr)
		whileGuardBExpr = BoolExpr(leftBExpr=orWhileGuardBExpr)
		#create the while statement
		equivalentWhileStatement = WhileStm.WhileStm(guard=whileGuardBExpr, stm=newStmOfWhile)
		atomEquivalentWhileStatement = AtomStatement.AtomStatement(equivalentWhileStatement)
		#execute the while statement
		atomEquivalentWhileStatement.executionProcess(state=state, listShowWhile=listShowWhile, listExecState=listExecState, flagReduction=True, variableSet=variableSet, flagAtomic=False)

	def calcNewVariableName(self, variableSet:Set[str]) -> str:
		#method to find the next name for the temporary variable
		#the name of the variable is created with a base name and an incrementasl index
		baseName = 'tmpVar'
		index = 0
		variableName = baseName + str(index)
		while variableName in variableSet:
			index += 1
			variableName = baseName + str(index)
		return variableName

	def executeNotReduction(self, state: Dict[str, float], listShowWhile: Union[List[str], None], listExecState: Union[List[str], None], variableSet: Set[str]) -> None:
		#the execution of a for statement whit the minimum logic: the while in a variable
		#populate the number of execution
		self.numberOfExecution = 0 #inizialize to 0 to contains the exact number of execution of the for
		#execute the start assignacion
		#it is necessary because the final value could be an expression of the variable
		startAssVar = AssignmentStm.AssignmentStm(var=self.var, aExp=self.arithmExprStart)
		startAssVar.executionProcess(state=state, listShowWhile=listShowWhile, listExecState=listExecState, flagReduction=False, variableSet=variableSet, flagAtomic=False)
		#now recall the same value
		startValue = self.var.evaluate(state=state, listExecState=listExecState, flagReduction=False)
		#evaluate the final value
		finalValue = self.arithmExprEnd.evaluateProcess(state=state, listExecState=listExecState, typeExpr=ExpressionBaseElement.typeEvalAExpr, flagReduction=True, flagAtomic=False)
		currentValue = startValue
		#iterate like the for
		while currentValue < finalValue:
			#at the first increment the number of execution
			self.numberOfExecution += 1
			#evaluate the var
			newAtomAExpr = AtomArithmExpr(currentValue)
			newAExpr = ArithmExpr([newAtomAExpr])
			varAssignStm = AssignmentStm.AssignmentStm(var=self.var, aExp=newAExpr)
			varAssAtomStm = AtomStatement.AtomStatement(varAssignStm)
			#execute it
			varAssAtomStm.executionProcess(state=state, listShowWhile=listShowWhile, listExecState=listExecState, flagReduction=False, variableSet=variableSet, flagAtomic=False)
			#execute the statement of the for
			self.stm.executionProcess(state=state, listShowWhile=listShowWhile, listExecState=listExecState, flagReduction=False, variableSet=variableSet, flagAtomic=False)
			#at the end increment the current value
			currentValue += 1

	#########################################################################################################################################################
	#method for graphic 

	def toGraphicTree(self):
		#method to convert the for statement to a graphic tree
		from PyQt5 import QtWidgets
		graphicTree = QtWidgets.QTreeWidgetItem()
		graphicTree.setText(0, 'ForStm')
		forChildTree = QtWidgets.QTreeWidgetItem()
		forChildTree.setText(0, 'for')
		varChildTree = self.var.toGraphicTree()
		forChildTree.addChild(varChildTree)
		fromChildTree = QtWidgets.QTreeWidgetItem()
		fromChildTree.setText(0, 'from')
		aritExprStartChildTree = self.arithmExprStart.toGraphicTree()
		fromChildTree.addChild(aritExprStartChildTree)
		toChildTree = QtWidgets.QTreeWidgetItem()
		toChildTree.setText(0, 'to')
		aritExprEndChildTree = self.arithmExprEnd.toGraphicTree()
		toChildTree.addChild(aritExprEndChildTree)
		doChildTree = QtWidgets.QTreeWidgetItem()
		doChildTree.setText(0, 'do')
		stmChildTree =  self.stm.toGraphicTree()
		doChildTree.addChild(stmChildTree)
		#add all to first item
		graphicTree.addChildren([forChildTree, fromChildTree, toChildTree, doChildTree])
		return graphicTree
		
