#script to implement the class of variables

from __future__ import annotations

from typing import Dict, List, Set, Union
from classes.ExpressionBaseElement import ExpressionBaseElement


class Variable(ExpressionBaseElement):
	#the class variable contains only the attribute for the name of the variable
	name : str

	def __init__(self, name) -> None:
		super().__init__()
		self.name = name

	def toStringTree(self, nTab=0) -> str:
		#method to convert the variable to a string
		stringTree = '\t'*nTab + 'Var:' +self.name + '\n'
		return stringTree

	def evaluate(self, state: Dict[str, float], listExecState: Union[List[str], None], flagReduction: bool) -> Union[float, bool]:
		#method to evaluate the variable
		#check if it in scope
		if self.name not in state:
			#exception
			raise Exception('Variable ' + self.name + ' not in scope')
		return state[self.name]
		

	def findVariableSet(self) -> Set[str]:
		return set( [self.name] )

	#########################################################################################################################################################
	#method for graphic 

	def toGraphicTree(self):
		#method to convert the variable to a graphic tree
		from PyQt5 import QtWidgets
		graphicTree = QtWidgets.QTreeWidgetItem()
		graphicTree.setText(0, 'Var')
		nameVarChildTree = QtWidgets.QTreeWidgetItem()
		nameVarChildTree.setText(0, self.name)
		graphicTree.addChild(nameVarChildTree)
		return graphicTree 
