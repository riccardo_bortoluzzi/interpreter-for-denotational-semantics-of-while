#contants for the execution

import os

constDirOut = os.path.join(os.getcwd(), 'output_main')

constNameParserTree = os.path.join(constDirOut, 'main_parser_tree.txt') #file that contains the tree parsed
constNameSummarizeExecutionNotRed = os.path.join(constDirOut, 'main_summarize_not_red.txt') #file that contains the summarize string of execution without reduction flag
constNameSummarizeExecutionRed = os.path.join(constDirOut, 'main_summarize_red.txt') #file that contains the summarize string of execution with reduction flag=True