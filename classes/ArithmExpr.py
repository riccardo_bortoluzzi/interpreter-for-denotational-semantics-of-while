#script to implement the arithmetic expression class

from __future__ import annotations
from classes.ExpressionBaseElement import ExpressionBaseElement

from typing import Dict, List, Set, Tuple, Union

import classes.AtomArithmExpr as AtomArithmExpr

class ArithmExpr(ExpressionBaseElement):

	listAtomExpression : List[Union[AtomArithmExpr.AtomArithmExpr, ArithmExpr]] #list that contains all the expression, if the expression is atomic it contains only a element
	listOperations : List[str] #list of operations to concatenate the expression, if the expression is atomic it is empty

	def __init__(self, listAtomAExpr : List[Union[AtomArithmExpr.AtomArithmExpr, ArithmExpr]], listOp:List[str] = [] ) -> None:
		#control in which case we are
		super().__init__()
		self.listAtomExpression = listAtomAExpr 
		self.listOperations = listOp
		if len(self.listAtomExpression) ==0:
			#error
			raise Exception('Input ArithmExpr not valid')

	def toStringTree(self, nTab=0) -> str:
		#method to convert the variable to a string
		#start with the number of tabs
		baseTab = '\t'*nTab
		stringTree = baseTab
		#check if there is only one expression
		if len(self.listAtomExpression) == 1:
			#only 1 expression, insert the rappresentation
			stringTree = self.listAtomExpression[0].toStringTree(nTab=nTab)
		else:
			stringTree += 'AExp[' + '\n'
			for i in range(len(self.listOperations)):
				stringTree += self.listAtomExpression[i].toStringTree(nTab=nTab+1)
				stringTree += '\t'*(nTab +1) + ' ' + self.listOperations[i] + ' ' + '\n'
			#at the end it is inserted the last element and the close square bracket
			stringTree += self.listAtomExpression[-1].toStringTree(nTab=nTab+1)
			stringTree += '\t'*nTab + ']' + '\n'
		#return the string
		return stringTree

	def findVariableSet(self) -> Set[str]:
		variableSet = set()
		for aExpr in self.listAtomExpression:
			variableSetAExpr = aExpr.findVariableSet()
			assert variableSetAExpr is not None
			variableSet.update(variableSetAExpr)
		return variableSet

	def evaluate(self, state: Dict[str, float], listExecState: Union[List[str], None], flagReduction:bool) -> float:
		#method to evaluate the arithmetic expression
		#no differences between Reduction and not
		isAtomic = len(self.listAtomExpression) == 1
		stringToEvaluate = ''
		for i in range(len(self.listOperations)):
			aExpr = self.listAtomExpression[i]
			opStr = self.listOperations[i]
			stringToEvaluate += str(aExpr.evaluateProcess(state=state, listExecState=listExecState, typeExpr=ExpressionBaseElement.typeEvalAExpr, flagAtomic=isAtomic, flagReduction=flagReduction))
			stringToEvaluate += opStr
		#the last expression is inserted
		stringToEvaluate+= str(self.listAtomExpression[-1].evaluateProcess(state=state, listExecState=listExecState, typeExpr=ExpressionBaseElement.typeEvalAExpr, flagAtomic=isAtomic, flagReduction=flagReduction))
		#now evaluate the string
		return eval(stringToEvaluate)

	#########################################################################################################################################################
	#method for graphic 

	def toGraphicTree(self):
		#method to convert the variable to a graphic tree
		from PyQt5 import QtWidgets
		#check if there is only one expression
		if len(self.listAtomExpression) == 1:
			#only 1 expression, insert the rappresentation
			graphicTree = self.listAtomExpression[0].toGraphicTree()
		else:
			graphicTree = QtWidgets.QTreeWidgetItem()
			graphicTree.setText(0, 'AExp')
			for i in range(len(self.listOperations)):
				childTree = self.listAtomExpression[i].toGraphicTree()
				graphicTree.addChild(childTree)
				opChild = QtWidgets.QTreeWidgetItem()
				opChild.setText(0, self.listOperations[i])
				graphicTree.addChild(opChild)
			#at the end it is inserted the last element 
			lastChildTree = self.listAtomExpression[-1].toGraphicTree()
			graphicTree.addChild(lastChildTree)
		#return the string
		return graphicTree




