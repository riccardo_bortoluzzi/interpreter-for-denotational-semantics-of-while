#script to implement the base element for the statement

from __future__ import annotations
from typing import Dict, List, Set, Union

from classes.BaseElement import BaseElement

class StatementBaseElement(BaseElement):

	def __init__(self) -> None:
		super().__init__()

	def executionProcess(self, state:Dict[str, float], listShowWhile:Union[List[str], None], listExecState : Union[list, None], flagReduction:bool, variableSet : Set[str], flagAtomic : bool ) -> None:
		#method to execute the statement, it uses side effect to state to compute the change of state
		#state: the current state at the begin of the execution
		#listShowWhile: list taht contains the string to show the number of execution to compute a while statement
		#listExecState: if it is not none, it a list that contains the string of the statement, the string of the initial state and the string that represents the final state
		#flagReduction : flag to execute the statement with Reductions (with possibility of recursion error) or without
		#setVariable is the set of all variables that are in the statement, it is useful for the forStatement
		#flagAtomic flag to indciate if the statement is atomic
		listExecution = None
		initialState = None
		if flagAtomic:
			listExecution = listExecState
		elif listExecState is not None:
			listExecution = []
			initialState = state.copy()
		self.execute(state= state, listShowWhile= listShowWhile, listExecState = listExecution,flagReduction=flagReduction, variableSet=variableSet)
		#at the end the final state is saved
		finalState = state.copy()
		if (listExecState is not None) and not(flagAtomic):
			#save the execution operation
			stringElement = self.toPlainString()
			stringExecution =  'Sds[[' + stringElement + ']] ' + str(initialState) + ' = ' + str(finalState)
			assert listExecution is not None
			listExecution.insert(0, stringExecution)
			listExecState.append(listExecution)
		

	def execute(self, state:Dict[str, float], listShowWhile:Union[List[str], None], listExecState : Union[List[str], None] , flagReduction:bool, variableSet : Set[str]) -> None:
		#method to execute the statement
		#will be overwritten in the implementation of child
		#to avoid not implementation this method raise exception
		raise Exception('method execute not implemented')