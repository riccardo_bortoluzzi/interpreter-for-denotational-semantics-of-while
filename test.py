#script to test funcion and classes

from classes.Statement import Statement
from classes.BoolExpr import BoolExpr
from classes.ArithmExpr import ArithmExpr
import os, re
from typing import Dict, List, Set, Tuple, Union
from classes.ExpressionBaseElement import ExpressionBaseElement
from classes.Parser import Parser

################################################################################################################################################################################################
#constants
constDirOut = os.path.join(os.getcwd(), 'output_test')
#constStrInput = '_input.txt' #name to the file that will contain the input
#constStrTree = '_tree.txt' #name of the file that will contain the tree parser
#constStrPlainTree = '_plain_tree.txt'#name of the file that will contain the plain tree
#constStrRemainString = '_remain.txt' #name of the file that will contain the remain string after parser
#constInitialState = '_initial_state.txt' #name of the file that will contain the initial state
#constStrFinalStateRed = '_final_state_red.txt' #name of the file that will contain the final state computed without Reduction
#constStrFinalStateNotRed = '_final_state_not_red.txt' #name of the file that will contain the final state computed with Reduction flag=True
#constStrResultValue = '_result_value.txt' #name of the file that will contain the final value computed without Reduction
#constStrExecTreeRed = '_exec_tree_red.txt' #name of the file that will contain the execution tree computed with Reduction flag
#constStrExecTreeNotRed = '_exec_tree_not_red.txt' #name of the file that will contain the execution tree computed with the Reduction flag at False
#constStrWhileRed = '_while_red.txt' #name of the file that will contain the while execution to Kleene-Knaster-Tarski fixpoint iteration sequence using Reduction flag=True
#constStrWhileNotRed = '_while_not_red.txt' #name of the file that will contain the while execution to Kleene-Knaster-Tarski fixpoint iteration sequence using Reduction flag=False
constStrSummarizeExecutionNotRed = '_summarize_not_red.txt' #file that contains the summarize string of execution without reduction flag
constStrSummarizeExecutionRed = '_summarize_red.txt' #file that contains the summarize string of execution with reduction flag=True

constTypeArithmeticExpression = 'A'
constTypeBoolExpression = 'B'
constTypeStatement = 'S'

def toStrExecListDepth(listExecution:list, depth:int=0) -> str:
	#function to return the string that represents the list in depth, every depth corresponding to an increment of tab
	strExec = ''
	for subList in listExecution:
		if type(subList) == list:
			#recursion
			strExec += toStrExecListDepth(listExecution=subList, depth=depth+1)
		else:
			#it is a string
			strExec += '\t'*(depth-1) + subList + '\n'
	return strExec


def executeTest(idKeyTest:str) -> None:
	#function to execute the test
	#create the base name file for the test
	baseNameFile = os.path.join(constDirOut, idKeyTest)
	parser = Parser()
	#first get the parameter
	(strInput, typeParser, state) = glDictTest[idKeyTest]
	#parse the expression
	if typeParser == constTypeArithmeticExpression:
		(tree, remain) = parser.parseArithmExpr(string=strInput)
	elif typeParser == constTypeBoolExpression:
		(tree, remain) = parser.parseBoolExpr(string=strInput)
	elif typeParser == constTypeStatement:
		(tree, remain) = parser.parseStatement(string=strInput)
	#create the string for the files
	stringTree = tree.toStringTree(nTab=0)
	stringPlainTree = tree.toPlainString()
	baseStringSummarize = 'Input:\n' + strInput + '\n\nTree parsed:\n' + stringTree + '\n\nRemain string:\n' + ('**Nothing**' if remain=='' else remain) + '\n\nExpression:\n' + typeParser + ': ' + stringPlainTree
	'''
	#now create the firsts file 
	with open(baseNameFile + constStrInput, 'w') as f:
		f.write(strInput)
	with open(baseNameFile + constStrTree, 'w') as f:
		f.write(stringTree)
	with open(baseNameFile + constStrPlainTree, 'w') as f:
		f.write(stringPlainTree)
	with open(baseNameFile + constStrRemainString, 'w') as f:
		f.write(remain)
	'''
	#prepare the execution
	initialState = state.copy()
	initialVariableSet = tree.findVariableSet()
	#without Reduction flag
	(resultNotRed, listWhileNotRed, listExecNotRed, stateNotRed, variableSetNotRed) = evaluateTree(tree=tree, iniSt=initialState, iniVarSet=initialVariableSet, flagRed=False)
	#create the string for summarize the execution with Reduction flag = False
	stringExecutionNotRed = toStrExecListDepth(listExecution=listExecNotRed, depth=0)
	stringWhileNotRed = ''.join(listWhileNotRed)
	stringSummarizeNotRed = 'I(' + typeParser + ', ' + str(initialState) + ') -> notRed = '
	if typeParser in (constTypeArithmeticExpression, constTypeBoolExpression):
		stringSummarizeNotRed += str(resultNotRed).lower()
	elif typeParser == constTypeStatement:
		stringSummarizeNotRed += str(stateNotRed)
	print(stringSummarizeNotRed)
	completeStringNotRed = baseStringSummarize + '\n\nExecution without reduce to simple case:\n' + stringSummarizeNotRed + '\n\n' + stringExecutionNotRed
	if len(stringWhileNotRed) >0:
		#there are some while statement
		completeStringNotRed += '\n\nWhile statements:\n' + stringWhileNotRed
	#now the file can be created
	with open(baseNameFile + constStrSummarizeExecutionNotRed, 'w') as f:
		f.write(completeStringNotRed)
	'''
	#create the second group of files
	with open(baseNameFile + constInitialState, 'w') as f:
		f.write(str(initialState))
	with open(baseNameFile + constStrFinalStateNotRed, 'w') as f:
		f.write(str(stateNotRed))
	with open(baseNameFile + constStrResultValue, 'w') as f:
		f.write(str(resultNotRed))
	with open(baseNameFile + constStrExecTreeNotRed, 'w') as f:
		f.write(stringExecutionNotRed)
	with open(baseNameFile + constStrWhileNotRed, 'w') as f:
		f.write(stringWhileNotRed)
	'''
	#the evaluation with reductions
	(resultRed, listWhileRed, listExecRed, stateRed, variableSetRed) = evaluateTree(tree=tree, iniSt=initialState, iniVarSet=initialVariableSet, flagRed=True)
	#create the string for summarize the execution with Reduction flag = True
	stringExecutionRed = toStrExecListDepth(listExecution=listExecRed, depth=0)
	stringWhileRed = ''.join(listWhileRed)
	stringSummarizeRed = 'I(' + typeParser + ', ' + str(initialState) + ') -> red = '
	if typeParser in (constTypeArithmeticExpression, constTypeBoolExpression):
		stringSummarizeRed += str(resultRed).lower()
	elif typeParser == constTypeStatement:
		stringSummarizeRed += str(stateRed)
	print(stringSummarizeRed)
	completeStringRed = baseStringSummarize + '\n\nExecution whit reduce to simple case:\n' + stringSummarizeRed + '\n\n' + stringExecutionRed
	if len(stringWhileRed) >0:
		#there are some while statement
		completeStringRed += '\n\nWhile statements:\n' + stringWhileRed
	#now the file can be created
	with open(baseNameFile + constStrSummarizeExecutionRed, 'w') as f:
		f.write(completeStringRed)
	'''
	#create the third group of files
	with open(baseNameFile + constStrExecTreeRed, 'w') as f:
		f.write(stringExecutionRed)
	with open(baseNameFile + constStrWhileRed, 'w') as f:
		f.write(stringWhileRed)
	with open(baseNameFile + constStrFinalStateRed, 'w') as f:
		f.write(str(stateRed))
	'''
	

def evaluateTree(tree:Union[ArithmExpr, BoolExpr, Statement], iniSt : Dict[str, float], iniVarSet : Set[str], flagRed:bool) -> Tuple[Union[bool, float, None], List[str], List[str], Dict[str, float], Set[str]]:
	#function to execute the tree
	listWhile = []
	listExec = []
	state = iniSt.copy()
	variableSet = iniVarSet.copy()
	#execute with not Reduction
	if isinstance(tree, ArithmExpr):
		#evaluate process
		result = tree.evaluateProcess(state=state, listExecState=listExec, typeExpr= ExpressionBaseElement.typeEvalAExpr, flagAtomic=False, flagReduction=flagRed)
	elif isinstance(tree, BoolExpr):
		result = tree.evaluateProcess(state=state, listExecState=listExec, typeExpr= ExpressionBaseElement.typeEvalBExpr, flagAtomic=False, flagReduction=flagRed)
	elif isinstance(tree, Statement):
		result = tree.executionProcess(state=state, listShowWhile=listWhile, listExecState=listExec, flagReduction=flagRed, variableSet=variableSet, flagAtomic=False)
	#return the tuple
	return (result, listWhile, listExec, state, variableSet)




glDictTest:Dict[str, Tuple[str, str, Dict[str, float]]] = {}
"""
parser = Parser()

stringToParseArtim = '''(0 - 54321*xfgt)    +
	jIns_6782 
- (1234 * 65341.7645 + xdf) * mng_
-((((ghjgjhg))))
+432'''

stringToParseBool = '''
(946.98 + (x*2)) <= yuiu88_&&
!x+3|=(hx*3 + (456 -8)) ||
(true 
	&&
	false
	||
	(xhy>poy+ytsr-987*9843.978 <-> falseflase == 8||true)
	)||
!(true && false)->
trdsh==0987*096||
false <-> x<=9
'''

stringToParseStatement = '''
x  := (1209.09 + (y*3) - (k+o-u));
if (x <= 76 && true || (1+9-8*(5) == 87) || !(false))
then
	skip
else
	while true
	do
		repeat
			for d:= (34+9*i) to hgt
			do
				skip;
				g:=9;
				h := g
			endstm
		until 
			x<=76 -> true
	endstm
endstm
		


'''
"""

glDictTest['AExprComplete'] = (
	'a +2*c - (4) + (a*a + 9*(a-c))',
	constTypeArithmeticExpression,
	{'a':2, 'A':3, 'c':5} #result: 2+2*5-4+(4+9*(2-5)) = -15
)

glDictTest['AExprEvaluationError'] = (
	'a +2*c',
	constTypeArithmeticExpression,
	{'a':2, 'A':3, 'C':5} #result: parse correct but error in evaluation
)

glDictTest['AExprSyntaxError1'] = (
	'a +2*',
	constTypeArithmeticExpression,
	{'a':2, 'A':3, 'C':5} #result: syntax error
)

glDictTest['AExprSyntaxError2'] = (
	'a +2*(-c)',
	constTypeArithmeticExpression,
	{'a':2, 'A':3, 'C':5} #result: syntax error
)

#create a test for all boolean expression

glDictTest['BExprEq1'] = (
	'x == 5',
	constTypeBoolExpression,
	{'x': 5} #result: true
)

glDictTest['BExprEq2'] = (
	'x == 5',
	constTypeBoolExpression,
	{'x': 4} #result: false
)

glDictTest['BExprNE'] = (
	'x |= 5',
	constTypeBoolExpression,
	{'x': 5} #result: false
)

glDictTest['BExprLT'] = (
	'x < 5',
	constTypeBoolExpression,
	{'x': 5} #result: false
)

glDictTest['BExprLE'] = (
	'x <= 5',
	constTypeBoolExpression,
	{'x': 5} #result: true
)

glDictTest['BExprGT'] = (
	'x > 5',
	constTypeBoolExpression,
	{'x': 5} #result: false
)

glDictTest['BExprGE'] = (
	'x >= 5',
	constTypeBoolExpression,
	{'x': 5} #result: true
)

glDictTest['BExprTrue'] = (
	'true',
	constTypeBoolExpression,
	{'x': 5} #result: true
)

glDictTest['BExprFalse'] = (
	'false',
	constTypeBoolExpression,
	{'x': 5} #result: false
)

glDictTest['BExprNot'] = (
	'!x==5',
	constTypeBoolExpression,
	{'x': 5} #result: false
)

glDictTest['BExprParenthesis'] = (
	'(!false)',
	constTypeBoolExpression,
	{'x': 5} #result: true
)

glDictTest['BExprAndFalse'] = (
	'true && x|=5',
	constTypeBoolExpression,
	{'x': 5} #result: false
)

glDictTest['BExprAndTrue'] = (
	'true && false',
	constTypeBoolExpression,
	{'x': 5} #result: false
)

glDictTest['BExprOr'] = (
	'false || true',
	constTypeBoolExpression,
	{'x': 5} #result: true
)

glDictTest['BExprPrecedenceAnd'] = (
	'true && false && false || true',
	constTypeBoolExpression,
	{'x': 5} #result: true (precedence of &&)
)

glDictTest['BExprSyntaxError'] = (
	'true && false &&  || true',
	constTypeBoolExpression,
	{'x': 5} #result: syntax error
)

glDictTest['BExprOrShortCircuit'] = (
	'true || a==7',
	constTypeBoolExpression,
	{'x': 5} #result: true (the arithmetic boolean expression will never be executed for short circuit)
)

glDictTest['BExprANdShortCircuit'] = (
	'false && a==7',
	constTypeBoolExpression,
	{'x': 5} #result: false (the arithmetic boolean expression will never be executed for short circuit)
)

glDictTest['BExprImpliesFalse'] = (
	'true -> false',
	constTypeBoolExpression,
	{'x': 5} #result: false
)

glDictTest['BExprImpliesTrue'] = (
	'false -> true',
	constTypeBoolExpression,
	{'x': 5} #result: true
)

glDictTest['BExprDoubleImpliesFalse'] = (
	'false <-> true',
	constTypeBoolExpression,
	{'x': 5} #result: false
)

glDictTest['BExprDoubleImpliesTrue'] = (
	'false <-> false',
	constTypeBoolExpression,
	{'x': 5} #result: true
)

glDictTest['BExprComplete'] = (
	'x > 0 && ( true || 4 < 7 ) && (!87>=32.09 || x==2) -> x|=2',
	constTypeBoolExpression,
	{'x': 2} #result: 2>0 && (true || 4<7) && (false || x==2) -> 2!=2  = true -> false = false
)

#test for statements

glDictTest['AssignStm'] = (
	'a := 5',
	constTypeStatement,
	{'x': 2} #result: {x:2, a:5}
)

glDictTest['skipStm'] = (
	'skip',
	constTypeStatement,
	{'x': 2} #result: {x:2}
)

glDictTest['ifTrueStm'] = (
	'if true then a:=1 else a:=2 end',
	constTypeStatement,
	{'x': 2} #result: {x:2, a:1}
)

glDictTest['ifFalseStm'] = (
	'if false then a:=1 else a:=2 end',
	constTypeStatement,
	{'x': 2} #result: {x:2, a:2}
)

glDictTest['infiniteLoopStm'] = (
	'while true do skip end',
	constTypeStatement,
	{'x': 2} #result: non termination
)

glDictTest['whileErrorReductionStm'] = (
	'while x<1500 do x := x+1 end',
	constTypeStatement,
	{'x': 0} #result: {x:1500} but the Reduction mode will raise exception: maximum number of recursion
)

glDictTest['whileStm'] = (
	'while x<3 do x := x+1 end',
	constTypeStatement,
	{'x': 0} #result: {x:3}
)

glDictTest['repeatStm'] = (
	'repeat x := x * (x+1) until x>10',
	constTypeStatement,
	{'x': 1} #result: {x:42}
)

glDictTest['forStm'] = (
	'for tmpVar0 := 1 to tmpVar0+10 do x := x + tmpVar0 end',
	constTypeStatement,
	{'x': 0} #result: {x:55, tmpVar0:10} #eventually with tmpVar1:11
)

glDictTest['forPotentiallyInfiniteStm'] = (
	'for x := 1 to 10 do x := x -1 end',
	constTypeStatement,
	{'x': 0} #result: {x:8} eventually with tmpVar0:10
)

glDictTest['strangeFactorial'] = (
	'''
	initial := 1;
	final := 10;
	correctFactorial := 1;
	oddFactorial := 1;
	isOdd := ((1));
	for i := initial to final+1
		do
			skip;
			if isOdd == 1
				then
					oddFactorial := oddFactorial * i
				else
					skip;
					skip
			end;
			isOdd := 1- isOdd;
			tmpCorrectFactorial := 0;
			ii := 0;
			while ii < i
				do 
					tmpCorrectFactorial := tmpCorrectFactorial + correctFactorial;
					ii := (ii+1)
			end;
			correctFactorial := tmpCorrectFactorial
	end'''
	,
	constTypeStatement,
	{} #result: {'initial': 1.0, 'final': 10.0, 'correctFactorial': 3628800.0, 'oddFactorial': 945.0, 'isOdd': 1.0, 'i': 10.0, 'tmpCorrectFactorial': 3628800.0, 'ii': 10.0} with eventually tmpVar0:11
)

glDictTest['strangeFactorial'] = (
	'''
	y := 0;
	while x<4 do
		y := y + 2*x; 
		x := x+1
	end'''
	,
	constTypeStatement,
	{'x': 0.0} 
)

"""
stringArithmExprToEvaluate = '''
a +2*c - (4) + (a*a + 9*(a-c))'''
stateAExprToEvaluate={'a':2, 'A':3, 'c':5} #result: 2+2*5-4+(4+9*(2-5)) = -15

stringBoolExprToEvaluate = '''
x > 0 && ( true || 4 < 7 ) && (!87>=32.09 || x==2) -> x|=2
'''
stateBExprToEvaluate={'x':2} #result: 2>0 && (true || 4<7) && (false || x==2) -> 2!=2  = true -> false = false

(aExpr, remain) = parser.parseArithmExpr(string=stringToParseArtim)
(bExpr, remain) = parser.parseBoolExpr(string=stringToParseBool)
(stm, remain) = parser.parseStatement(string=stringToParseStatement)
(aExprEval, remain) = parser.parseArithmExpr(string=stringArithmExprToEvaluate)
(bExprEval, remain) = parser.parseBoolExpr(string=stringBoolExprToEvaluate)


f=open('outTree.txt', 'w')

#print(aExpr.toStringTree(nTab=0))
#print(bExpr.toStringTree(nTab=0))
#print(stm.toStringTree(nTab=0))
#print(aExprEval.toStringTree(nTab=0))
print(bExprEval.toStringTree(nTab=0))

#print(bExpr.toPlainString())

f.write(bExprEval.toStringTree(nTab=0))
f.write('\n---------------------------------------------------------------------------------------------------------------------------------------------------------------------\n')
f.write(bExprEval.toPlainString())
f.close()

#ricordarsi #sys.setrecursionlimit(1000000)

#evaluation
listExecution = []
valueResult = bExprEval.evaluateProcess(state=stateBExprToEvaluate, listExecState=listExecution, typeExpr=ExpressionBaseElement.typeEvalBExpr, flagAtomic=False, flagReduction=True)
#print all passages
printListDepth(listExecution=listExecution, depth=0)
#at the end print the value
print('final value:', valueResult)
"""

#for first clean the directory
for i in os.listdir(constDirOut):
	pathFile = os.path.join(constDirOut, i)
	os.remove(pathFile)

for k in glDictTest:
	print(k + ': '+ re.sub(pattern=r'\s+', repl=' ', string=glDictTest[k][0]) )
	if k not in ('infiniteLoopStm',):
		try:
			executeTest(idKeyTest=k)
		except Exception as e:
			print(e)
		print('')

#executeTest(idKeyTest='BExprParenthesis')

#print(glDictTest)
