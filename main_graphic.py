#script main for execute the parse with GUI

import sys
from PyQt5 import QtCore, QtGui, QtWidgets

from classes.MainWindowSemantics import MainWindowSemantics

def main_graphic():
	app = QtWidgets.QApplication(sys.argv)
	MainWindow = MainWindowSemantics()
	MainWindow.show()
	sys.exit(app.exec_())

if __name__ == '__main__':
	main_graphic()
