# Interpreter for the denotational semantics of While+

This is the project for an exam of Software Verification. The professor is Francesco Ranzato of University of Padua.
It implements an interpreter for the denotational semantics of While+.

There are 2 version of the program: 
1. the version that interacts to the user with command line;
2. the version that interact to the user with a Gui in Qt.

## Command line version

For the command line version, execute the command
```
python3 main.py
```

The program asks:
1. if yout want to parse an arithmetic expression (`A`), a boolean expression (`B`) or a complete statement (`S`);
2. if you want to write the string to parse (`I`) or read it from a file (`F`);
3. the string to parse (in one line) or the path of the file;
4. if you want to simulate the execution with reducion (`R`), with minimum number of steps (`M`) or with both of all (`A`);
5. the initial state of variables in format `var_1:float_1, var_2:float_2...`

Then the program will print in the console the parsed tree and the simulation of execution. Files will be created into the directory `output_main`.

## Graphic interface version

For the graphic version, PyQt5 is required

```
pip3 install PyQt5
```

You can run the program with
```
python3 main_graphic.py
```

Also this script creates files in directory `output_main`.

### Input tab
![Input tab](images/main_window.png)

In the `Input` tab you can:
- choose what do you want to parse;
- write the string to parse also in multiline format or read it from a file and modify it later;
- choose how to simulate the execution;
- set the maximum number of reductions, more reductions will raise a recursion error;
- modify the list of variables of initial state with buttons `Add Variable` and `Remove Variable`;
- choose to only parse the string, without simulation of execution with button `Only Parse` or execute the full process with parse and simulation with button `Parse and Execution` 

### Parsed Tree tab

![Parsed tree](images/parsed_tree.png)

In the tab `Parsed` you can see the parsed tree and remain string after parse.

### Execution tab

![Execution](images/execution.png)

In the tab `Execution` you can see:
- the initial state;
- the final state;
- the minimum _n_ that realize the Kleene-Knaster-Tarski's iteration;
- the tree of simulation of execution with reduction, if setted;
- the tree of simulation of execution with minimum number of steps, if setted. 

## Test script
The test script
```
python3 test.py
```

parses and simulates the execution with bot techniques of different possible input strings, except for these could raise a recursion error. 
It creates files in the directory `output_test` with the information about parsed tree and simulations of execution.

For simplicity, the directory `input_tests` contains files with the input strings analized by this script. 

## What can be parsed
The program can parse:
- Arithmetic expression;
- Boolean expression;
- Complete statement.

When the program is executed, the file `main_parser_tree.txt` is created into the directory `output_main` and it contains the tree parsed anc the remain string.

### Grammar
The grammar for the expression.
- Numerals:
	- it represents any numbers, also decimals numbers (the decimal separator is dot `.`).
	- Symbol: `n`
- Variables:
	- it represents variables, variable name starts with a letter and contains letters, digits and underscore.
	- Symbol: `x`
- Atomic arithmhmetic expression:
	- Symbol: `AtomAE`
	- Grammar:
		- `n`
		- `x`
		- `(AExpr)`
- Arithmhmetic expression 
	- Symbol: `AExpr`
	- Grammar:
		- `AtomAE`
		- `AtomAE_1 op_1 AtomAE_2 op_2 ... op_n AtomAE_n+1` with `op_i` ∈ {`+`, `*`, `-`}
- Aritmetic boolean expression 
	- Symbol: `AritBE`
	- Grammar:
		- `AExpr_1 == AExpr_2`
		- `AExpr_1 |= AExpr_2`
		- `AExpr_1 < AExpr_2`
		- `AExpr_1 <= AExpr_2`
		- `AExpr_1 > AExpr_2`
		- `AExpr_1 >= AExpr_2`

- Atomic boolean expression
	- Symbol: `AtomBE`
	- Grammar:
		- `true`
		- `false`
		- `! AtomBE`
		- `(BExpr)`
		- `AritBE`

- Boolean expression with _and_:
	- Symbol: `AndBE`
	- Grammar:
		- `AtomBE`
		- `AtomBE && AndBE`

- Boolean expression with _or_:
	- Symbol: `OrBE`
	- Grammar:
		- `AndBE`
		- `AndBE || OrBE`

- Boolean expression:
	- Symbol: `BExpr`
	- Grammar:
		- `OrBE`
		 - `OrBE -> BExpr`
		 - `OrBE <-> BExpr`

- Atomic statement
	- Symbol: `AtomS`
	- Grammar:
		- `skip`
		- `x := AExpr`
		- `if BExpr then Stm_1 else Stm_2 end`
		- `while BExpr do Stm end`
		- `repeat Stm until BExpr`	
		- `for x := AExpr_1 to AExpr_2 do Stm end`

- Statement
	- Symbol: `Stm`
	- Grammar:
		- `AtomS`
		- `AtomS_1; AtomS_2; ... AtomS_n-1; AtomS_n`

## How can be executed
When the program is executed, the files `main_summarize_red.txt` and `main_summarize_not_red.txt` could be created into the directory `output_main` and they contains simulation of the execution of the statement.

In the rows of files will be indicated:
1. the expression or the statement;
2. the initial state in curly brackets;
3. the result or the final state in curly brackets.

The execution of the statements can be simulated:
- with reduction to simpler case (with no termination statements, a maximum recursion error is raised) (file `main_summarize_red.txt`);
- with the minimum number of steps (with no termination statements, the program run forever) (file `main_summarize_not_red.txt`);
- with both of theese 2 techniques.

### Reductions
The program, when a statement is executed with reduction, implements these transformations:
|**Expression**|**Reduction**|
|:------|:------|
|`a_1 \|= a_2]` | `! a_1 == a_2` |
|`a_1 < a_2]` | `a_1 <= a_2 && a_1 \|= a_2` |
|`a_1 > a_2]` | `! a_1 <= a_2` |
|`a_1 >= a_2]` | `! a_1 <= a_2 \|\| a_1 == a_2` |
|`b_1 -> b_2]` | `! b_1 \|\| b_2` |
|`b_1 <-> b_2]` | `b_1 && b_2 \|\| ! b_1 && ! b_2` |
|`b_1 \|\| a_2]` | `! ( !b_1 && !b_2 )` |

|**Statement**|**Reduction**|
|:------|:------|
|`while b do S end` | `if b then S; while b do S else skip end` |
|`repeat S until b` | `S; while ! ( b ) do S` |
|`for x := a_1 to a_2 do S end` | `tVar := a_1; while tVar < a_2 do x := tVar; S; tVar := tVar + 1 end` |
