Input:
(!false)

Tree parsed:
BExp[
	(
		BExp[
			Not[
				Bool:False
			]
		]
	)
]


Remain string:
**Nothing**

Expression:
B: BExp[(BExp[Not[Bool:False]])]

Execution whit reduce to simple case:
I(B, {'x': 5}) -> red = true

B[[BExp[(BExp[Not[Bool:False]])]]] {'x': 5} = true
	B[[BExp[Not[Bool:False]]]] {'x': 5} = true
		B[[Bool:False]] {'x': 5} = false
