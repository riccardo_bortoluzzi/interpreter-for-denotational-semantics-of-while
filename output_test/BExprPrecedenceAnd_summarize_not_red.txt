Input:
true && false && false || true

Tree parsed:
BExp[
	BExp[
		Bool:True
		 && 
		BExp[
			Bool:False
			 && 
			Bool:False
		]
	]
	 || 
	Bool:True
]


Remain string:
**Nothing**

Expression:
B: BExp[BExp[Bool:True && BExp[Bool:False && Bool:False]] || Bool:True]

Execution without reduce to simple case:
I(B, {'x': 5}) -> notRed = true

B[[BExp[BExp[Bool:True && BExp[Bool:False && Bool:False]] || Bool:True]]] {'x': 5} = true
	B[[BExp[Bool:True && BExp[Bool:False && Bool:False]]]] {'x': 5} = false
		B[[Bool:True]] {'x': 5} = true
		B[[BExp[Bool:False && Bool:False]]] {'x': 5} = false
			B[[Bool:False]] {'x': 5} = false
	B[[Bool:True]] {'x': 5} = true
