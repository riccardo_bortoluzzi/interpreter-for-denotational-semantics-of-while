Input:
false <-> true

Tree parsed:
BExp[
	Bool:False
	 <-> 
	Bool:True
]


Remain string:
**Nothing**

Expression:
B: BExp[Bool:False <-> Bool:True]

Execution without reduce to simple case:
I(B, {'x': 5}) -> notRed = false

B[[BExp[Bool:False <-> Bool:True]]] {'x': 5} = false
	B[[Bool:False]] {'x': 5} = false
	B[[Bool:True]] {'x': 5} = true
