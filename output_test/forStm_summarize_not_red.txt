Input:
for tmpVar0 := 1 to tmpVar0+10 do x := x + tmpVar0 end

Tree parsed:
Stm[
	for 
		Var:tmpVar0
	 from 
		Num:1.0
	 to 
		AExp[
			Var:tmpVar0
			 + 
			Num:10.0
		]
	 do 
		Stm[
			Ass[
				Var:x
				 := 
				AExp[
					Var:x
					 + 
					Var:tmpVar0
				]
			]
		]
]


Remain string:
**Nothing**

Expression:
S: Stm[for Var:tmpVar0 from Num:1.0 to AExp[Var:tmpVar0 + Num:10.0] do Stm[Ass[Var:x := AExp[Var:x + Var:tmpVar0]]]]

Execution without reduce to simple case:
I(S, {'x': 0}) -> notRed = {'x': 55.0, 'tmpVar0': 10.0}

Sds[[Stm[for Var:tmpVar0 from Num:1.0 to AExp[Var:tmpVar0 + Num:10.0] do Stm[Ass[Var:x := AExp[Var:x + Var:tmpVar0]]]]]] {'x': 0} = {'x': 55.0, 'tmpVar0': 10.0}
	Sds[[for Var:tmpVar0 from Num:1.0 to AExp[Var:tmpVar0 + Num:10.0] do Stm[Ass[Var:x := AExp[Var:x + Var:tmpVar0]]]]] {'x': 0} = {'x': 55.0, 'tmpVar0': 10.0}
		Sds[[Ass[Var:tmpVar0 := Num:1.0]]] {'x': 0} = {'x': 0, 'tmpVar0': 1.0}
			A[[Num:1.0]] {'x': 0} = 1.0
		A[[AExp[Var:tmpVar0 + Num:10.0]]] {'x': 0, 'tmpVar0': 1.0} = 11.0
			A[[Var:tmpVar0]] {'x': 0, 'tmpVar0': 1.0} = 1.0
				V[[Var:tmpVar0]] {'x': 0, 'tmpVar0': 1.0} = 1.0
			A[[Num:10.0]] {'x': 0, 'tmpVar0': 1.0} = 10.0
		Sds[[Stm[Ass[Var:tmpVar0 := Num:1.0]]]] {'x': 0, 'tmpVar0': 1.0} = {'x': 0, 'tmpVar0': 1.0}
			Sds[[Ass[Var:tmpVar0 := Num:1.0]]] {'x': 0, 'tmpVar0': 1.0} = {'x': 0, 'tmpVar0': 1.0}
				A[[Num:1.0]] {'x': 0, 'tmpVar0': 1.0} = 1.0
		Sds[[Stm[Ass[Var:x := AExp[Var:x + Var:tmpVar0]]]]] {'x': 0, 'tmpVar0': 1.0} = {'x': 1.0, 'tmpVar0': 1.0}
			Sds[[Ass[Var:x := AExp[Var:x + Var:tmpVar0]]]] {'x': 0, 'tmpVar0': 1.0} = {'x': 1.0, 'tmpVar0': 1.0}
				A[[AExp[Var:x + Var:tmpVar0]]] {'x': 0, 'tmpVar0': 1.0} = 1.0
					A[[Var:x]] {'x': 0, 'tmpVar0': 1.0} = 0
						V[[Var:x]] {'x': 0, 'tmpVar0': 1.0} = 0
					A[[Var:tmpVar0]] {'x': 0, 'tmpVar0': 1.0} = 1.0
						V[[Var:tmpVar0]] {'x': 0, 'tmpVar0': 1.0} = 1.0
		Sds[[Stm[Ass[Var:tmpVar0 := Num:2.0]]]] {'x': 1.0, 'tmpVar0': 1.0} = {'x': 1.0, 'tmpVar0': 2.0}
			Sds[[Ass[Var:tmpVar0 := Num:2.0]]] {'x': 1.0, 'tmpVar0': 1.0} = {'x': 1.0, 'tmpVar0': 2.0}
				A[[Num:2.0]] {'x': 1.0, 'tmpVar0': 1.0} = 2.0
		Sds[[Stm[Ass[Var:x := AExp[Var:x + Var:tmpVar0]]]]] {'x': 1.0, 'tmpVar0': 2.0} = {'x': 3.0, 'tmpVar0': 2.0}
			Sds[[Ass[Var:x := AExp[Var:x + Var:tmpVar0]]]] {'x': 1.0, 'tmpVar0': 2.0} = {'x': 3.0, 'tmpVar0': 2.0}
				A[[AExp[Var:x + Var:tmpVar0]]] {'x': 1.0, 'tmpVar0': 2.0} = 3.0
					A[[Var:x]] {'x': 1.0, 'tmpVar0': 2.0} = 1.0
						V[[Var:x]] {'x': 1.0, 'tmpVar0': 2.0} = 1.0
					A[[Var:tmpVar0]] {'x': 1.0, 'tmpVar0': 2.0} = 2.0
						V[[Var:tmpVar0]] {'x': 1.0, 'tmpVar0': 2.0} = 2.0
		Sds[[Stm[Ass[Var:tmpVar0 := Num:3.0]]]] {'x': 3.0, 'tmpVar0': 2.0} = {'x': 3.0, 'tmpVar0': 3.0}
			Sds[[Ass[Var:tmpVar0 := Num:3.0]]] {'x': 3.0, 'tmpVar0': 2.0} = {'x': 3.0, 'tmpVar0': 3.0}
				A[[Num:3.0]] {'x': 3.0, 'tmpVar0': 2.0} = 3.0
		Sds[[Stm[Ass[Var:x := AExp[Var:x + Var:tmpVar0]]]]] {'x': 3.0, 'tmpVar0': 3.0} = {'x': 6.0, 'tmpVar0': 3.0}
			Sds[[Ass[Var:x := AExp[Var:x + Var:tmpVar0]]]] {'x': 3.0, 'tmpVar0': 3.0} = {'x': 6.0, 'tmpVar0': 3.0}
				A[[AExp[Var:x + Var:tmpVar0]]] {'x': 3.0, 'tmpVar0': 3.0} = 6.0
					A[[Var:x]] {'x': 3.0, 'tmpVar0': 3.0} = 3.0
						V[[Var:x]] {'x': 3.0, 'tmpVar0': 3.0} = 3.0
					A[[Var:tmpVar0]] {'x': 3.0, 'tmpVar0': 3.0} = 3.0
						V[[Var:tmpVar0]] {'x': 3.0, 'tmpVar0': 3.0} = 3.0
		Sds[[Stm[Ass[Var:tmpVar0 := Num:4.0]]]] {'x': 6.0, 'tmpVar0': 3.0} = {'x': 6.0, 'tmpVar0': 4.0}
			Sds[[Ass[Var:tmpVar0 := Num:4.0]]] {'x': 6.0, 'tmpVar0': 3.0} = {'x': 6.0, 'tmpVar0': 4.0}
				A[[Num:4.0]] {'x': 6.0, 'tmpVar0': 3.0} = 4.0
		Sds[[Stm[Ass[Var:x := AExp[Var:x + Var:tmpVar0]]]]] {'x': 6.0, 'tmpVar0': 4.0} = {'x': 10.0, 'tmpVar0': 4.0}
			Sds[[Ass[Var:x := AExp[Var:x + Var:tmpVar0]]]] {'x': 6.0, 'tmpVar0': 4.0} = {'x': 10.0, 'tmpVar0': 4.0}
				A[[AExp[Var:x + Var:tmpVar0]]] {'x': 6.0, 'tmpVar0': 4.0} = 10.0
					A[[Var:x]] {'x': 6.0, 'tmpVar0': 4.0} = 6.0
						V[[Var:x]] {'x': 6.0, 'tmpVar0': 4.0} = 6.0
					A[[Var:tmpVar0]] {'x': 6.0, 'tmpVar0': 4.0} = 4.0
						V[[Var:tmpVar0]] {'x': 6.0, 'tmpVar0': 4.0} = 4.0
		Sds[[Stm[Ass[Var:tmpVar0 := Num:5.0]]]] {'x': 10.0, 'tmpVar0': 4.0} = {'x': 10.0, 'tmpVar0': 5.0}
			Sds[[Ass[Var:tmpVar0 := Num:5.0]]] {'x': 10.0, 'tmpVar0': 4.0} = {'x': 10.0, 'tmpVar0': 5.0}
				A[[Num:5.0]] {'x': 10.0, 'tmpVar0': 4.0} = 5.0
		Sds[[Stm[Ass[Var:x := AExp[Var:x + Var:tmpVar0]]]]] {'x': 10.0, 'tmpVar0': 5.0} = {'x': 15.0, 'tmpVar0': 5.0}
			Sds[[Ass[Var:x := AExp[Var:x + Var:tmpVar0]]]] {'x': 10.0, 'tmpVar0': 5.0} = {'x': 15.0, 'tmpVar0': 5.0}
				A[[AExp[Var:x + Var:tmpVar0]]] {'x': 10.0, 'tmpVar0': 5.0} = 15.0
					A[[Var:x]] {'x': 10.0, 'tmpVar0': 5.0} = 10.0
						V[[Var:x]] {'x': 10.0, 'tmpVar0': 5.0} = 10.0
					A[[Var:tmpVar0]] {'x': 10.0, 'tmpVar0': 5.0} = 5.0
						V[[Var:tmpVar0]] {'x': 10.0, 'tmpVar0': 5.0} = 5.0
		Sds[[Stm[Ass[Var:tmpVar0 := Num:6.0]]]] {'x': 15.0, 'tmpVar0': 5.0} = {'x': 15.0, 'tmpVar0': 6.0}
			Sds[[Ass[Var:tmpVar0 := Num:6.0]]] {'x': 15.0, 'tmpVar0': 5.0} = {'x': 15.0, 'tmpVar0': 6.0}
				A[[Num:6.0]] {'x': 15.0, 'tmpVar0': 5.0} = 6.0
		Sds[[Stm[Ass[Var:x := AExp[Var:x + Var:tmpVar0]]]]] {'x': 15.0, 'tmpVar0': 6.0} = {'x': 21.0, 'tmpVar0': 6.0}
			Sds[[Ass[Var:x := AExp[Var:x + Var:tmpVar0]]]] {'x': 15.0, 'tmpVar0': 6.0} = {'x': 21.0, 'tmpVar0': 6.0}
				A[[AExp[Var:x + Var:tmpVar0]]] {'x': 15.0, 'tmpVar0': 6.0} = 21.0
					A[[Var:x]] {'x': 15.0, 'tmpVar0': 6.0} = 15.0
						V[[Var:x]] {'x': 15.0, 'tmpVar0': 6.0} = 15.0
					A[[Var:tmpVar0]] {'x': 15.0, 'tmpVar0': 6.0} = 6.0
						V[[Var:tmpVar0]] {'x': 15.0, 'tmpVar0': 6.0} = 6.0
		Sds[[Stm[Ass[Var:tmpVar0 := Num:7.0]]]] {'x': 21.0, 'tmpVar0': 6.0} = {'x': 21.0, 'tmpVar0': 7.0}
			Sds[[Ass[Var:tmpVar0 := Num:7.0]]] {'x': 21.0, 'tmpVar0': 6.0} = {'x': 21.0, 'tmpVar0': 7.0}
				A[[Num:7.0]] {'x': 21.0, 'tmpVar0': 6.0} = 7.0
		Sds[[Stm[Ass[Var:x := AExp[Var:x + Var:tmpVar0]]]]] {'x': 21.0, 'tmpVar0': 7.0} = {'x': 28.0, 'tmpVar0': 7.0}
			Sds[[Ass[Var:x := AExp[Var:x + Var:tmpVar0]]]] {'x': 21.0, 'tmpVar0': 7.0} = {'x': 28.0, 'tmpVar0': 7.0}
				A[[AExp[Var:x + Var:tmpVar0]]] {'x': 21.0, 'tmpVar0': 7.0} = 28.0
					A[[Var:x]] {'x': 21.0, 'tmpVar0': 7.0} = 21.0
						V[[Var:x]] {'x': 21.0, 'tmpVar0': 7.0} = 21.0
					A[[Var:tmpVar0]] {'x': 21.0, 'tmpVar0': 7.0} = 7.0
						V[[Var:tmpVar0]] {'x': 21.0, 'tmpVar0': 7.0} = 7.0
		Sds[[Stm[Ass[Var:tmpVar0 := Num:8.0]]]] {'x': 28.0, 'tmpVar0': 7.0} = {'x': 28.0, 'tmpVar0': 8.0}
			Sds[[Ass[Var:tmpVar0 := Num:8.0]]] {'x': 28.0, 'tmpVar0': 7.0} = {'x': 28.0, 'tmpVar0': 8.0}
				A[[Num:8.0]] {'x': 28.0, 'tmpVar0': 7.0} = 8.0
		Sds[[Stm[Ass[Var:x := AExp[Var:x + Var:tmpVar0]]]]] {'x': 28.0, 'tmpVar0': 8.0} = {'x': 36.0, 'tmpVar0': 8.0}
			Sds[[Ass[Var:x := AExp[Var:x + Var:tmpVar0]]]] {'x': 28.0, 'tmpVar0': 8.0} = {'x': 36.0, 'tmpVar0': 8.0}
				A[[AExp[Var:x + Var:tmpVar0]]] {'x': 28.0, 'tmpVar0': 8.0} = 36.0
					A[[Var:x]] {'x': 28.0, 'tmpVar0': 8.0} = 28.0
						V[[Var:x]] {'x': 28.0, 'tmpVar0': 8.0} = 28.0
					A[[Var:tmpVar0]] {'x': 28.0, 'tmpVar0': 8.0} = 8.0
						V[[Var:tmpVar0]] {'x': 28.0, 'tmpVar0': 8.0} = 8.0
		Sds[[Stm[Ass[Var:tmpVar0 := Num:9.0]]]] {'x': 36.0, 'tmpVar0': 8.0} = {'x': 36.0, 'tmpVar0': 9.0}
			Sds[[Ass[Var:tmpVar0 := Num:9.0]]] {'x': 36.0, 'tmpVar0': 8.0} = {'x': 36.0, 'tmpVar0': 9.0}
				A[[Num:9.0]] {'x': 36.0, 'tmpVar0': 8.0} = 9.0
		Sds[[Stm[Ass[Var:x := AExp[Var:x + Var:tmpVar0]]]]] {'x': 36.0, 'tmpVar0': 9.0} = {'x': 45.0, 'tmpVar0': 9.0}
			Sds[[Ass[Var:x := AExp[Var:x + Var:tmpVar0]]]] {'x': 36.0, 'tmpVar0': 9.0} = {'x': 45.0, 'tmpVar0': 9.0}
				A[[AExp[Var:x + Var:tmpVar0]]] {'x': 36.0, 'tmpVar0': 9.0} = 45.0
					A[[Var:x]] {'x': 36.0, 'tmpVar0': 9.0} = 36.0
						V[[Var:x]] {'x': 36.0, 'tmpVar0': 9.0} = 36.0
					A[[Var:tmpVar0]] {'x': 36.0, 'tmpVar0': 9.0} = 9.0
						V[[Var:tmpVar0]] {'x': 36.0, 'tmpVar0': 9.0} = 9.0
		Sds[[Stm[Ass[Var:tmpVar0 := Num:10.0]]]] {'x': 45.0, 'tmpVar0': 9.0} = {'x': 45.0, 'tmpVar0': 10.0}
			Sds[[Ass[Var:tmpVar0 := Num:10.0]]] {'x': 45.0, 'tmpVar0': 9.0} = {'x': 45.0, 'tmpVar0': 10.0}
				A[[Num:10.0]] {'x': 45.0, 'tmpVar0': 9.0} = 10.0
		Sds[[Stm[Ass[Var:x := AExp[Var:x + Var:tmpVar0]]]]] {'x': 45.0, 'tmpVar0': 10.0} = {'x': 55.0, 'tmpVar0': 10.0}
			Sds[[Ass[Var:x := AExp[Var:x + Var:tmpVar0]]]] {'x': 45.0, 'tmpVar0': 10.0} = {'x': 55.0, 'tmpVar0': 10.0}
				A[[AExp[Var:x + Var:tmpVar0]]] {'x': 45.0, 'tmpVar0': 10.0} = 55.0
					A[[Var:x]] {'x': 45.0, 'tmpVar0': 10.0} = 45.0
						V[[Var:x]] {'x': 45.0, 'tmpVar0': 10.0} = 45.0
					A[[Var:tmpVar0]] {'x': 45.0, 'tmpVar0': 10.0} = 10.0
						V[[Var:tmpVar0]] {'x': 45.0, 'tmpVar0': 10.0} = 10.0


While statements:
F: Stm[for Var:tmpVar0 from Num:1.0 to AExp[Var:tmpVar0 + Num:10.0] do Stm[Ass[Var:x := AExp[Var:x + Var:tmpVar0]]]]
s: {'x': 0}
s': {'x': 55.0, 'tmpVar0': 10.0}
F^10(⊥) s = s'

