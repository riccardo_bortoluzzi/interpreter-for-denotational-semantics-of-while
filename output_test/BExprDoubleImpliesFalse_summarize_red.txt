Input:
false <-> true

Tree parsed:
BExp[
	Bool:False
	 <-> 
	Bool:True
]


Remain string:
**Nothing**

Expression:
B: BExp[Bool:False <-> Bool:True]

Execution whit reduce to simple case:
I(B, {'x': 5}) -> red = false

B[[BExp[Bool:False <-> Bool:True]]] {'x': 5} = false
	B[[BExp[BExp[BExp[(Bool:False)] && BExp[(Bool:True)]] || BExp[BExp[Not[BExp[(Bool:True)]]] && BExp[Not[BExp[(Bool:False)]]]]]]] {'x': 5} = false
		B[[BExp[Not[BExp[(BExp[BExp[Not[BExp[(BExp[BExp[(Bool:False)] && BExp[(Bool:True)]])]]] && BExp[Not[BExp[(BExp[BExp[Not[BExp[(Bool:True)]]] && BExp[Not[BExp[(Bool:False)]]]])]]]])]]]]] {'x': 5} = false
			B[[BExp[(BExp[BExp[Not[BExp[(BExp[BExp[(Bool:False)] && BExp[(Bool:True)]])]]] && BExp[Not[BExp[(BExp[BExp[Not[BExp[(Bool:True)]]] && BExp[Not[BExp[(Bool:False)]]]])]]]])]]] {'x': 5} = true
				B[[BExp[BExp[Not[BExp[(BExp[BExp[(Bool:False)] && BExp[(Bool:True)]])]]] && BExp[Not[BExp[(BExp[BExp[Not[BExp[(Bool:True)]]] && BExp[Not[BExp[(Bool:False)]]]])]]]]]] {'x': 5} = true
					B[[BExp[Not[BExp[(BExp[BExp[(Bool:False)] && BExp[(Bool:True)]])]]]]] {'x': 5} = true
						B[[BExp[(BExp[BExp[(Bool:False)] && BExp[(Bool:True)]])]]] {'x': 5} = false
							B[[BExp[BExp[(Bool:False)] && BExp[(Bool:True)]]]] {'x': 5} = false
								B[[BExp[(Bool:False)]]] {'x': 5} = false
									B[[Bool:False]] {'x': 5} = false
					B[[BExp[Not[BExp[(BExp[BExp[Not[BExp[(Bool:True)]]] && BExp[Not[BExp[(Bool:False)]]]])]]]]] {'x': 5} = true
						B[[BExp[(BExp[BExp[Not[BExp[(Bool:True)]]] && BExp[Not[BExp[(Bool:False)]]]])]]] {'x': 5} = false
							B[[BExp[BExp[Not[BExp[(Bool:True)]]] && BExp[Not[BExp[(Bool:False)]]]]]] {'x': 5} = false
								B[[BExp[Not[BExp[(Bool:True)]]]]] {'x': 5} = false
									B[[BExp[(Bool:True)]]] {'x': 5} = true
										B[[Bool:True]] {'x': 5} = true
