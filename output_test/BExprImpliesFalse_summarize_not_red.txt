Input:
true -> false

Tree parsed:
BExp[
	Bool:True
	 -> 
	Bool:False
]


Remain string:
**Nothing**

Expression:
B: BExp[Bool:True -> Bool:False]

Execution without reduce to simple case:
I(B, {'x': 5}) -> notRed = false

B[[BExp[Bool:True -> Bool:False]]] {'x': 5} = false
	B[[Bool:True]] {'x': 5} = true
	B[[Bool:False]] {'x': 5} = false
