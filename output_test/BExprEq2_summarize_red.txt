Input:
x == 5

Tree parsed:
ABExp[
	Var:x
	 == 
	Num:5.0
]


Remain string:
**Nothing**

Expression:
B: ABExp[Var:x == Num:5.0]

Execution whit reduce to simple case:
I(B, {'x': 4}) -> red = false

B[[ABExp[Var:x == Num:5.0]]] {'x': 4} = false
	A[[Var:x]] {'x': 4} = 4
		V[[Var:x]] {'x': 4} = 4
	A[[Num:5.0]] {'x': 4} = 5.0
