Input:
!x==5

Tree parsed:
BExp[
	Not[
		ABExp[
			Var:x
			 == 
			Num:5.0
		]
	]
]


Remain string:
**Nothing**

Expression:
B: BExp[Not[ABExp[Var:x == Num:5.0]]]

Execution whit reduce to simple case:
I(B, {'x': 5}) -> red = false

B[[BExp[Not[ABExp[Var:x == Num:5.0]]]]] {'x': 5} = false
	B[[ABExp[Var:x == Num:5.0]]] {'x': 5} = true
		A[[Var:x]] {'x': 5} = 5
			V[[Var:x]] {'x': 5} = 5
		A[[Num:5.0]] {'x': 5} = 5.0
